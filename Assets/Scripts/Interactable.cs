using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    [SerializeField]
    protected GameManager gameManager;
    PlayerInteraction inter;
    public GameObject popup;
    public bool canInteract = true;
    public bool inCollider = false;

    protected virtual void Start()
    {
        if (GetComponent<CircleCollider2D>() == null)
        {
            gameObject.AddComponent<CircleCollider2D>();
        }

        popup = GameObject.Instantiate(Resources.Load<GameObject>("InteractionPopup"), transform);
        popup.transform.rotation = Quaternion.identity;
        popup.transform.localScale = Vector3.one;
        popup.transform.localScale = new Vector3(popup.transform.localScale.x / popup.transform.lossyScale.x, popup.transform.localScale.y / popup.transform.lossyScale.y, popup.transform.localScale.z / popup.transform.lossyScale.z);
        popup.SetActive(false);

        gameManager = GameManager.Instance;
    }

    public virtual void Interact()
    {
        popup.SetActive(false);
    }

    public virtual void Update()
    {
        if (inCollider && !inter.interactables.Contains(this))
        {
            inter.interactables.Add(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        inter = collision.GetComponent<PlayerInteraction>();
        collision.GetComponent<PlayerInteraction>().interactables.Add(this);
        inCollider = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        inCollider = false;
        collision.GetComponent<PlayerInteraction>().interactables.Remove(this);
        popup.SetActive(false);
    }
}
