using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class PauseMenu : MonoBehaviour
{
    bool startWasPress = false;

    public void ResumeButtonPressed()
    {
        GameManager.Instance.PauseGame();
        gameObject.transform.localScale = Vector3.zero;
    }

    public void OptionsButtonPressed()
    {
        //Debug.Log("No options menu yet");
    }

    public void MenuButtonPressed()
    {
        SceneManager.LoadScene("Title Scene", LoadSceneMode.Single);
    }

    public void QuitButtonPressed()
    {
        //Debug.Log("quit");
        Application.Quit();
    }

    void Update()
    {
        if (!GameManager.Instance.gameIsPaused)
        {
            gameObject.transform.localScale = Vector3.zero;
        }
        else
        {
            gameObject.transform.localScale = Vector3.one * 1.75f;
        }

        if (Input.GetKeyDown(KeyCode.Escape) || (Gamepad.current != null && Gamepad.current.startButton.isPressed && !startWasPress))
        {
            GameManager.Instance.PauseGame();
        }

        if (!GameManager.Instance.gameIsPaused && !Application.isFocused)
        {
            GameManager.Instance.PauseGame();
        }

        startWasPress = (Gamepad.current != null && Gamepad.current.startButton.isPressed);
    }
}
