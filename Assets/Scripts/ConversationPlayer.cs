using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.VisualScripting;

public class ConversationPlayer : MonoBehaviour
{
    [SerializeField]
    RawImage characterImage;
    [SerializeField]
    RawImage acc;
    [SerializeField]
    TextMeshProUGUI characterText;
    [SerializeField]
    AudioSource textAudio;

    public Conversation conversation;
    public bool revealingText = false;
    public bool preventInput = false;
    int revealedText = 0;
    int currentTalkIndex = 0;
    CharacterTalk currentTalk;
    Coroutine talkCoroutine;
    public System.Action callback;

    void Start()
    {
        transform.localScale = Vector3.one;
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && conversation.skippable)
        {
            revealingText = !revealingText;

            if (revealingText)
            {
                if (currentTalkIndex < conversation.characterTalks.Count - 1)
                {
                    PlayNextTalk();
                } else
                {
                    callback();
                    ResetCallback();
                    if (GameManager.Instance != null)
                    {
                        GameManager.Instance.UpdatePlayerState(PlayerState.Walking);
                    }
                    gameObject.SetActive(false);
                }
            } else
            {
                StopCoroutine(talkCoroutine);
                characterText.text = currentTalk.text;
            }
        }
    }

    public void ForceNextTalk()
    {
        StopCoroutine(talkCoroutine);
        if (currentTalkIndex < conversation.characterTalks.Count - 1)
        {
            PlayNextTalk();
        }
        else
        {
            callback();
            ResetCallback();
            GameManager.Instance.UpdatePlayerState(PlayerState.Walking);
            gameObject.SetActive(false);
        }
        //Debug.Log(currentTalkIndex);
    }

    public void PlayConversation(Conversation newConversation)
    {
        gameObject.SetActive(true);
        conversation = newConversation;
        currentTalkIndex = -1;
        PlayNextTalk();
    }

    public void PlayNextTalk()
    {
        currentTalkIndex++;

        if (currentTalkIndex >= conversation.characterTalks.Count)
        {
            callback();
            ResetCallback();
            GameManager.Instance.UpdatePlayerState(PlayerState.Walking);
            gameObject.SetActive(false);
            return;
        }

        currentTalk = conversation.characterTalks[currentTalkIndex];
        characterImage.texture = currentTalk.characterHead;
        if (currentTalk.acc != null)
        {
            //acc.gameObject.SetActive(true);
            acc.texture = currentTalk.acc;
            acc.color = Color.white;
        }
        else
        {
            acc.color = Color.clear;
            //acc.gameObject.SetActive(false);
        }
        characterText.color = currentTalk.color;
        characterText.text = "";
        revealedText = 0;
        talkCoroutine = StartCoroutine(TextPlayer());
    }

    IEnumerator TextPlayer()
    {
        while (revealedText < currentTalk.text.Length)
        {
            if (revealedText >= currentTalk.text.Length)
            {
                break;
            }

            yield return new WaitForSeconds(currentTalk.speed);
            textAudio.PlayOneShot(currentTalk.charSound);
            characterText.text += currentTalk.text[revealedText];
            revealedText++;
        }
        revealingText = false;
    }

    public void ResetCallback()
    {
        callback = new System.Action(DoNothing);
    }

    public void DoNothing()
    {

    }
}
