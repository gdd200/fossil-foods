using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class InteractionIndicatior : MonoBehaviour
{
    GameManager gameManager;

    public SpriteRenderer PCMouse;
    public SpriteRenderer PCKeyboard;
    public SpriteRenderer Xbox;
    public SpriteRenderer Playstation;
    public SpriteRenderer Switch;

#pragma warning disable 0618
    void Start()
    {
        gameManager = GameManager.Instance;
        //GameManager.Instance.playerChangedInput += ChangeInput;
        PCMouse.gameObject.active = PCKeyboard.gameObject.active = Xbox.gameObject.active = Playstation.gameObject.active = Switch.gameObject.active = false;
        PCMouse.gameObject.active = PCKeyboard.gameObject.active = true;
    }

    private void OnApplicationQuit()
    {
        //GameManager.Instance.playerChangedInput -= ChangeInput;
    }

    private void OnDestroy()
    {
        //GameManager.Instance.playerChangedInput -= ChangeInput;
    }

    private void Update()
    {
        if (gameManager.lastUsedInput == PlayerInputDevice.PC)
        {
            float opacity = Mathf.Cos(Time.time) / 2f + 0.5f;
            PCMouse.color = new Color(1f, 1f, 1f, opacity * 1.5f);
            PCKeyboard.color = new Color(1f, 1f, 1f, (1 - opacity) * 1.5f);
        }
    }

    void ChangeInput(PlayerInputDevice device)
    {
        if (SceneManager.GetActiveScene().name != "Restaurant" && SceneManager.GetActiveScene().name != "Kitchen") return;

        PCMouse.gameObject.active = PCKeyboard.gameObject.active = Xbox.gameObject.active = Playstation.gameObject.active = Switch.gameObject.active = false;

        switch (device)
        {
            case PlayerInputDevice.PC:
                PCMouse.gameObject.active = PCKeyboard.gameObject.active = true;
                break;

            case PlayerInputDevice.Xbox:
                Xbox.gameObject.active = true;
                break;

            case PlayerInputDevice.Playstation:
                Playstation.gameObject.active = true;
                break;

            case PlayerInputDevice.Switch:
                Switch.gameObject.active = true;
                break;
        }
    }
}
