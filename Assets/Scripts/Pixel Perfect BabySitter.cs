using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

[ExecuteAlways]
public class PixelPerfectBabySitter : MonoBehaviour
{
    PixelPerfectCamera ppC;
    void Start()
    {
        ppC = GetComponent<PixelPerfectCamera>();
    }

    private void FixedUpdate()
    {
        ppC.assetsPPU = Mathf.RoundToInt(Mathf.Min(Screen.width / 1920f, Screen.height / 1080f) * 192);
    }
}
