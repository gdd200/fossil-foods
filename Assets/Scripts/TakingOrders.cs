using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakingOrders : MonoBehaviour
{
    GameManager gameManager;

    void Start()
    {
        transform.localScale = Vector3.one;
        gameObject.SetActive(false);
        gameManager = GameManager.Instance;
    }

    private void Update()
    {
        if (!gameManager.conversationPlayer.gameObject.activeInHierarchy)
        {
            gameObject.SetActive(false);
        }
    }
}
