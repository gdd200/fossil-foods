using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{
    public bool finishedInto = false;
    public bool takenOrder = false;
    public bool finishedTake = false;
    public bool droppedOffFood = false;
    public bool finishedWait = false;
    public bool pickedUpFood = false;
    public bool deliverFood = false;
    public bool finishedDeliver = false;
    public bool finishedEnd = false;

    GameManager gameManager;
    ConversationPlayer conversationPlayer;
    public List<Conversation> tutorialConversations = new List<Conversation>();
    public int playedTutorials = 0;

    void Start()
    {
        gameManager = GameManager.Instance;
        conversationPlayer = gameManager.conversationPlayer;
        Invoke("StartTutorial", 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void StartTutorial()
    {
        StartCoroutine(DoTutorial());
    }

    IEnumerator DoTutorial()
    {
        conversationPlayer.callback = new System.Action(FinishedInto);
        conversationPlayer.PlayConversation(tutorialConversations[playedTutorials]);
        playedTutorials++;

        while (!finishedInto)
        {
            yield return null;
        }

        gameManager.patronSpawner.SpawnPatron();

        while (!takenOrder)
        {
            foreach (PatronTable table in Resources.FindObjectsOfTypeAll<PatronTable>())
            {
                if (table.busy && table.needsFood && !table.needsToOrder && gameManager.player.takingOrders.Count > 0)
                {
                    takenOrder = true;
                }
            }

            yield return null;
        }

        conversationPlayer.callback = new System.Action(FinishedTake);
        conversationPlayer.PlayConversation(tutorialConversations[playedTutorials]);
        playedTutorials++;

        while (!finishedTake)
        {
            yield return null;
        }

        while (!droppedOffFood)
        {
            if (gameManager.player.takingOrders.Count == 0)
            {
                droppedOffFood = true;
            }

            yield return null;
        }

        conversationPlayer.callback = new System.Action(FinishedWait);
        conversationPlayer.PlayConversation(tutorialConversations[playedTutorials]);
        playedTutorials++;

        while (!finishedWait)
        {
            yield return null;
        }

        while (!pickedUpFood)
        {
            if (gameManager.player.deliveringFood.Count > 0)
            {
                pickedUpFood = true;
            }

            yield return null;
        }

        conversationPlayer.callback = new System.Action(FinishedDeliver);
        conversationPlayer.PlayConversation(tutorialConversations[playedTutorials]);
        playedTutorials++;

        while (!deliverFood)
        {
            yield return null;
        }

        while (!finishedDeliver)
        {
            bool allGone = true;
            foreach (PatronTable table in Resources.FindObjectsOfTypeAll<PatronTable>())
            {
                if (table.busy)
                {
                    allGone = false;
                }
            }

            if (allGone)
            {
                finishedDeliver = true;
            }

            yield return null;
        }

        conversationPlayer.callback = new System.Action(FinishedEnd);
        conversationPlayer.PlayConversation(tutorialConversations[playedTutorials]);
        playedTutorials++;

        while (!finishedEnd)
        {
            yield return null;
        }

        SceneManager.LoadScene("Restaurant");
    }

    public void FinishedInto()
    {
        finishedInto = true;
    }

    public void FinishedTake()
    {
        finishedTake = true;
    }

    public void FinishedWait()
    {
        finishedWait = true;
    }

    public void FinishedDeliver()
    {
        deliverFood = true;
    }

    public void FinishedEnd()
    {
        finishedEnd = true;
    }
}
