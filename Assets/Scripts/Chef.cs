using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chef : MonoBehaviour
{
    public static Chef Instance;
    List<CookingRecipie> needToCook = new List<CookingRecipie>();
    public List<Recipes> cooked = new List<Recipes>();
    public int maxCookAtOneTime = 6;
    public AudioSource foodComplete;
    public AudioClip alertClip;
    public GameObject exclamationPoint;
    public Transform cookingDisplay;
    public float cookTime = 10f;

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        } else
        {
            Destroy(gameObject);
        }
        GameManager.Instance.chef = this;
    }

    public void AddDish(Recipes recipe)
    {
        needToCook.Insert(0, new CookingRecipie(recipe, cookTime));
    }

    void Update()
    {
        int cooking = 0;
        for (int i = needToCook.Count - 1; i >= 0; i--)
        {
            needToCook[i].cookTimer -= Time.deltaTime;

            if (needToCook[i].cookTimer <= 0)
            {
                cooked.Add(needToCook[i].dish);
                needToCook.RemoveAt(i);
                foodComplete.PlayOneShot(alertClip);
            }

            cooking++;

            if (cooking > maxCookAtOneTime)
            {
                return;
            }
        }
        if (cooked.Count > 0)
        {
            if (kitchen.kitchenExclamationPoint != null)
            {
                kitchen.kitchenExclamationPoint.SetActive(true);
            }
        }
        else
        {
            if (kitchen.kitchenExclamationPoint != null)
            {
                kitchen.kitchenExclamationPoint.SetActive(false);
            }
        }

        for (int i = 0; i < cookingDisplay.childCount; i++)
        {
            if (i >= needToCook.Count)
            {
                cookingDisplay.GetChild(i).gameObject.SetActive(false);
                continue;
            }

            cookingDisplay.GetChild(i).gameObject.SetActive(true);
            cookingDisplay.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>(needToCook[i].dish.GetSpritePath());
            cookingDisplay.GetChild(i).GetChild(0).GetComponent<RectMask2D>().padding = new Vector4(0, 0, 0, 64 * (needToCook[i].cookTimer / cookTime));
            cookingDisplay.GetChild(i).GetChild(0).GetChild(0).GetComponent<Image>().sprite = cookingDisplay.GetChild(i).GetComponent<Image>().sprite;
        }
    }
}

public class CookingRecipie
{
    public Recipes dish;
    public float cookTimer;

    public CookingRecipie (Recipes recipe, float time)
    {
        cookTimer = time;
        dish = recipe;
    }
}