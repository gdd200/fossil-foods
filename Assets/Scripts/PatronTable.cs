using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PatronTable : Interactable
{
    public List<PatronSeat> seats;
    public List<Patron> patronGroup;
    public List<Patron> patrons;
    public TextMesh debugText;
    public bool needsToOrder = false;
    public bool needsFood = false;
    public bool busy = false;

    public GameObject exclamationPoint;
    public Vector3 exclamationPos;
    public Vector2 defaultExclamationPos;

    public GameObject uiExclamation;

    void Start()
    {
        base.Start();
        canInteract = false;

        seats.AddRange(GetComponentsInChildren<PatronSeat>());
        debugText = GetComponentInChildren<TextMesh>();
        debugText.transform.localPosition = new Vector3(0, 1.5f, 0);

        exclamationPoint = new GameObject();
        exclamationPoint.name = "Exclamation Point";
        exclamationPoint.transform.parent = gameObject.transform;
        exclamationPoint.AddComponent<SpriteRenderer>();
        exclamationPoint.transform.position = gameObject.transform.position + Vector3.up;
        exclamationPoint.GetComponent<SpriteRenderer>().color = Color.red;
        exclamationPoint.GetComponent<SpriteRenderer>().sortingLayerName = "Patience Meter";

        defaultExclamationPos = exclamationPoint.transform.position;
        exclamationPoint.SetActive(true);

        exclamationPos = Camera.main.WorldToScreenPoint(exclamationPoint.transform.position);

        uiExclamation = new GameObject();
        uiExclamation.name = "UI Exclamation";
        uiExclamation.transform.parent = GameManager.Instance.canvas;
        uiExclamation.AddComponent<Image>();
        uiExclamation.GetComponent<Image>().color = Color.red;
        uiExclamation.GetComponent<Image>().sprite = Resources.Load<Sprite>("VanHornMatthias_ExclimationPoint_01");
        uiExclamation.SetActive(false);
    }

    public void CheckReady()
    {
        bool ready = true;

        for (int i = 0; i < patronGroup.Count; i++)
        {
            if (!ready)
            {
                continue;
            }

            if (patrons.IndexOf(patronGroup[i]) == -1)
            {
                ready = false;
            }
        }

        needsToOrder = ready;
        canInteract = ready;

        gameManager.player.ticketCount = PlayerUpgradeManager.Instance.ticketCount + PlayerUpgradeManager.Instance.ticketIncrement * PlayerUpgradeManager.Instance.ticketLevel;
    }

    public void FixedUpdate()
    {
        if (needsFood && !needsToOrder)
        {
            exclamationPoint.GetComponent<SpriteRenderer>().sprite = null;
        }

        if (false && needsToOrder && gameManager.player.servedTables >= gameManager.player.ticketCount && !needsFood)
        {
            canInteract = false;
        } else if (needsToOrder)
        {
            canInteract = true;
            exclamationPoint.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("VanHornMatthias_ExclimationPoint_01");
        }

        if (needsFood && gameManager.player.deliveringFood.Count >= patronGroup.Count)
        {
            canInteract = true;
        } else if (needsFood)
        {
            canInteract = false;
            exclamationPoint.GetComponent<SpriteRenderer>().sprite = null;
        }

        string debugString = "";

        foreach (Patron patron in patronGroup)
        {
            if (patron != null)
            {
                debugString += Vector3.Distance(patron.transform.position, transform.position) + "\n";
            }
            else
            {
                debugString += "null\n";
            }
        }
        //debugText.text = debugString;


        bool allGone = true;

        foreach (PatronSeat seat in seats)
        {
            if (seat.occupied)
            {
                allGone = false;
            }
        }

        if (!allGone) return;
        //debugText.text = "ALL GONE";

        patrons.Clear();
        patronGroup.Clear();
        busy = false;

        //Vector2 screenVector = Camera.main.WorldToScreenPoint(defaultExclamationPos);
        //bool isOffscreen = screenVector.x <= 0 || screenVector.x >= Camera.main.pixelWidth
        //    || screenVector.y <= 0 || screenVector.y >= Camera.main.pixelHeight;
        //if (isOffscreen)
        //{
        //    float newX = Mathf.Clamp(screenVector.x, 0, Camera.main.pixelWidth);
        //    float newY = Mathf.Clamp(screenVector.x, 0, Camera.main.pixelHeight);
        //    exclamationPoint.transform.position = Camera.main.ScreenToWorldPoint(new Vector2(newX, newY));
        //}
        //else
        //{
        //    exclamationPoint.transform.position = defaultExclamationPos;
        //}
        
        //if (isOffscreen && exclamationPoint.activeSelf)
        //{
        //    uiExclamation.SetActive(true);
        //    Vector2 directionVector = exclamationPoint.transform.position - GameManager.Instance.player.transform.position;
        //    Vector2 viewportDirection = Camera.main.WorldToViewportPoint(directionVector);
        //    float newX = Mathf.Clamp(viewportDirection.x, 0, 1);
        //    float newY = Mathf.Clamp(viewportDirection.y, 0, 1);
        //    Vector2 newPos = new Vector2(newX, newY);
        //    uiExclamation.transform.position = Camera.main.ViewportToScreenPoint(newPos);
        //}
        //else
        //{
        //    uiExclamation.SetActive(false);
        //}
    }

    public override void Interact()
    {
        if (needsFood)
        {
            List<Recipes> takenRecipes = new List<Recipes>();

            bool hasAllFood = true;

            foreach (Patron patron in patronGroup)
            {
                if (gameManager.player.deliveringFood.Contains(patron.order))
                {
                    gameManager.player.deliveringFood.Remove(patron.order);
                    StartCoroutine(OnFinishGetFood(patron, true));
                }
                else
                {
                    hasAllFood = false;
                    StartCoroutine(OnFinishGetFood(patron, false));
                }
            }

            if (!hasAllFood)
            {
                gameManager.player.health -= 1;
            }


            canInteract = false;
            needsFood = false;
        }

        if (needsToOrder)
        {
            List<Recipes> recipes = new List<Recipes>(0);
            Conversation orderConversation = (Conversation)ScriptableObject.CreateInstance(typeof(Conversation));
            orderConversation.skippable = false;
            orderConversation.characterTalks = new List<CharacterTalk>(0);

            foreach (Patron patron in patrons)
            {
                patron.state = Patron.State.Talking;
                orderConversation.characterTalks.Add(patron.orderTalk);
                patron.order.origin = this;
                //Debug.Log(patron.order.origin);
                recipes.Add(patron.order);
            }

            gameManager.foodMenu.SetFoodSprites(recipes);
            gameManager.UpdatePlayerState(PlayerState.TakingOrder);
            gameManager.conversationPlayer.PlayConversation(orderConversation);
            gameManager.conversationPlayer.callback = new System.Action(OnFinishOrder);
            gameManager.takingOrders.gameObject.SetActive(true);
            gameManager.player.servedTables++;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            Vector2 viewportCenter = ((Vector2)gameManager.foodMenu.transform.position + new Vector2(-12, -54)) / Camera.main.pixelRect.size;
            Vector2 fixedCenter = new Vector2(Mathf.Abs(viewportCenter.x), Mathf.Abs(viewportCenter.y));
            Vector2 notepadPos = Camera.main.ViewportToScreenPoint(fixedCenter);
            Mouse.current.WarpCursorPosition(notepadPos);

            needsToOrder = false;
            needsFood = true;
            canInteract = true;
            
        }

        base.Interact();
    }

    public void OnFinishOrder()
    {
        foreach (Patron patron in patrons)
        {
            patron.state = Patron.State.Waiting;
        }
    }

    public IEnumerator OnFinishGetFood(Patron patron, bool correct)
    {
        GameObject eatparticles = gameObject; 

        if (correct)
        {
            eatparticles = GameObject.Instantiate(Resources.Load<GameObject>(patron.order.GetEatPath()));
            eatparticles.transform.position = transform.position +  (patron.transform.position - transform.position) / 5f;
            eatparticles.transform.position += Vector3.back * 5;
        }

        yield return new WaitForSeconds(2f);

        if (correct)
        {
            patron.GiveTip();
            Destroy(eatparticles);
        }

        patron.Leave();
    }
}
