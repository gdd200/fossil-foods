using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PatienceBar : MonoBehaviour
{
    public SpriteRenderer background;
    public SpriteRenderer bar;
    public float barMax = 1f;
    public float barMin = 0f;

    public void SetBar(float percent)
    {
        float size = Mathf.Lerp(barMin, barMax, percent);
        size *= 100;
        size = Mathf.CeilToInt(size);
        size /= 100;
        bar.size = new Vector2(size, 0.09f);
    }
}
