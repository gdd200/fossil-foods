using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class FoodMenu : RadialMenu
{
    public List<Image> options = new List<Image>(0);
    public List<Dish> dishes = new List<Dish>(0);
    public List<Image> dishImages0 = new List<Image>(0);
    public List<Image> dishImages1 = new List<Image>(0);
    public List<Image> dishImages2 = new List<Image>(0);
    public List<Image> dishImages3 = new List<Image>(0);
    public List<List<Image>> dishImages = new List<List<Image>>(0);
    public List<List<Recipes>> dishPickings = new List<List<Recipes>>(0);
    public List<Recipes> debugRecipies = new List<Recipes>(0);

    public int selected = -1;
    public RawImage highlightCircle;
    public float inputMagnitude = 0.1f;

    public bool selectingIngredient = false;
    public int selectedOption = 0;
    public Vector2 mouseMovement = Vector2.zero;
    public Vector2 lastMousePos = Vector2.zero;
    public RectTransform rectTransform;
    public RectTransform debugCursor;

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        SetNumberOfOptions(options.Count);

        base.Start();
        deadZone = inputMagnitude;

        dishes.Add(Dish.Soup);
        dishes.Add(Dish.Soup);
        dishes.Add(Dish.Soup);
        dishes.Add(Dish.Soup);

        dishPickings.Add(new List<Recipes>(0));
        dishPickings.Add(new List<Recipes>(0));
        dishPickings.Add(new List<Recipes>(0));
        dishPickings.Add(new List<Recipes>(0));

        debugRecipies.Add(Recipes.GenerateDish());
        debugRecipies.Add(Recipes.GenerateDish());
        debugRecipies.Add(Recipes.GenerateDish());
        debugRecipies.Add(Recipes.GenerateDish());

        dishImages = new List<List<Image>>() { dishImages0, dishImages1, dishImages2, dishImages3 };
    }

    private void OldUpdate()
    {
        if (!selectingIngredient)
        {
            handleMouseSelf = true;
            base.Update();

            Vector3 mouseOnScreen = Camera.main.ScreenToViewportPoint(Mouse.current.position.value) * Camera.main.pixelRect.size;
            float distanceTo = Mathf.Infinity;

            for (int i = 0; i < options.Count; i++)
            {
                if (Vector3.Distance(options[i].transform.position, mouseOnScreen) < distanceTo)
                {
                    currentHighlight = i;
                    distanceTo = Vector3.Distance(options[i].transform.position, mouseOnScreen);
                }
            }

            HighlightOption(currentHighlight);
            return;
        }

        if (Input.GetButtonDown("Fire2") || (Gamepad.current != null && Gamepad.current.bButton.isPressed))
        {
            totalMouseMovement = Vector2.zero;
            Vector2 viewportCenter1 = ((Vector2)gameManager.foodMenu.transform.position + new Vector2(-12, -54)) / Camera.main.pixelRect.size;
            Vector2 fixedCenter1 = new Vector2(Mathf.Abs(viewportCenter1.x), Mathf.Abs(viewportCenter1.y));
            Vector2 notepadPos1 = Camera.main.ViewportToScreenPoint(fixedCenter1);
            Mouse.current.WarpCursorPosition(notepadPos1);

            selectingIngredient = false;
            return;
        }

        foreach (Image im2 in GetImagesInChildren(options[selectedOption].transform.GetChild(0).gameObject))
        {
            im2.color = new Color(1, 1, 1, 1f);
        }
        foreach (Image im2 in GetImagesInChildren(options[selectedOption].transform.GetChild(1).gameObject))
        {
            im2.color = new Color(1, 1, 1, 1f);
        }

        int hover = 0;

        if (gameManager.lastUsedInput == PlayerInputDevice.PC)
        {
            Vector2 input = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
            totalMouseMovement += (Vector2)Camera.main.ScreenToViewportPoint(Mouse.current.position.value - lastMousePos) * Camera.main.pixelRect.size;

            switch (dishPickings[selectedOption].Count)
            {
                case 2:
                    /*if (totalMouseMovement.y > 0)
                    {
                        hover = 0;
                        highlightCircle.transform.position = options[selectedOption].transform.GetChild(0).GetChild(0).position;
                    } else
                    {
                        hover = 1;
                        highlightCircle.transform.position = options[selectedOption].transform.GetChild(0).GetChild(1).position;
                    }*/

                    Vector3 mouseOnScreen0 = Camera.main.ScreenToViewportPoint(Mouse.current.position.value) * Camera.main.pixelRect.size;
                    hover = 0;
                    float dist0 = Vector2.Distance(options[selectedOption].transform.GetChild(0).GetChild(0).position, mouseOnScreen0);

                    if (Vector2.Distance(options[selectedOption].transform.GetChild(0).GetChild(1).position, mouseOnScreen0) < dist0)
                    {
                        hover = 1;
                        dist0 = Vector2.Distance(options[selectedOption].transform.GetChild(0).GetChild(1).position, mouseOnScreen0);
                    }

                    highlightCircle.transform.position = options[selectedOption].transform.GetChild(0).GetChild(hover).position;
                    break;

                case 3:
                    Vector3 mouseOnScreen = Camera.main.ScreenToViewportPoint(Mouse.current.position.value) * Camera.main.pixelRect.size;
                    hover = 0;
                    float dist = Vector2.Distance(options[selectedOption].transform.GetChild(1).GetChild(0).position, mouseOnScreen);

                    if (Vector2.Distance(options[selectedOption].transform.GetChild(1).GetChild(1).position, mouseOnScreen) < dist)
                    {
                        hover = 1;
                        dist = Vector2.Distance(options[selectedOption].transform.GetChild(1).GetChild(1).position, mouseOnScreen);
                    }

                    if (Vector2.Distance(options[selectedOption].transform.GetChild(1).GetChild(2).position, mouseOnScreen) < dist)
                    {
                        hover = 2;
                        dist = Vector2.Distance(options[selectedOption].transform.GetChild(1).GetChild(2).position, mouseOnScreen);
                    }

                    highlightCircle.transform.position = options[selectedOption].transform.GetChild(1).GetChild(hover).position;

                    /* if (totalMouseMovement.y > dishImages[selectedOption][selectedOption].rectTransform.sizeDelta.y)
                    {
                        hover = 0;
                        highlightCircle.transform.position = options[selectedOption].transform.GetChild(1).GetChild(0).position;
                    }
                    else if (totalMouseMovement.y > -dishImages[selectedOption][selectedOption].rectTransform.sizeDelta.y)
                    {
                        hover = 1;
                        highlightCircle.transform.position = options[selectedOption].transform.GetChild(1).GetChild(1).position;
                    }
                    else
                    {
                        hover = 2;
                        highlightCircle.transform.position = options[selectedOption].transform.GetChild(1).GetChild(2).position;
                    }*/
                    break;
            }
        }
        else
        {
            Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

            switch (dishPickings[selectedOption].Count)
            {
                case 2:
                    if (input.y > 0)
                    {
                        hover = 0;
                        highlightCircle.transform.position = options[selectedOption].transform.GetChild(0).GetChild(0).position;
                    }
                    else
                    {
                        hover = 1;
                        highlightCircle.transform.position = options[selectedOption].transform.GetChild(0).GetChild(1).position;
                    }
                    break;

                case 3:
                    if (input.y > 0.333f / 2f)
                    {
                        hover = 0;
                        highlightCircle.transform.position = options[selectedOption].transform.GetChild(1).GetChild(0).position;
                    }
                    else if (input.y > -0.333f / 2f)
                    {
                        hover = 1;
                        highlightCircle.transform.position = options[selectedOption].transform.GetChild(1).GetChild(1).position;
                    }
                    else
                    {
                        hover = 2;
                        highlightCircle.transform.position = options[selectedOption].transform.GetChild(1).GetChild(2).position;
                    }
                    break;
            }
        }

        if (Input.GetButtonDown("Fire1") && !GameManager.Instance.conversationPlayer.revealingText)
        {
            selectingIngredient = false;
            gameManager.player.takingOrders.Add(dishPickings[selectedOption][hover]);
            gameManager.conversationPlayer.ForceNextTalk();

            totalMouseMovement = Vector2.zero;
            Vector2 viewportCenter1 = ((Vector2)gameManager.foodMenu.transform.position + new Vector2(-12, -54)) / Camera.main.pixelRect.size;
            Vector2 fixedCenter1 = new Vector2(Mathf.Abs(viewportCenter1.x), Mathf.Abs(viewportCenter1.y));
            Vector2 notepadPos1 = Camera.main.ViewportToScreenPoint(fixedCenter1);
            Mouse.current.WarpCursorPosition(notepadPos1);

            foreach (Image im in options)
            {
                im.transform.GetChild(0).gameObject.SetActive(false);
                im.transform.GetChild(1).gameObject.SetActive(false);
            }
        }

        lastMousePos = Mouse.current.position.value;
    }

    private void Update()
    {
        if (Time.timeScale < 0.01f) return;

        handleMouseSelf = true;

        Vector2 mouseOnScreen = (Vector2)(Camera.main.ScreenToViewportPoint(Mouse.current.position.value) * Camera.main.pixelRect.size);
        Vector2 center = rectTransform.position;
        Vector2 mouseRelative = mouseOnScreen - center; 
        float rightEdge = (rectTransform.rect.width * transform.lossyScale.x) / 2f;
        float leftEdge = (-rectTransform.rect.width * transform.lossyScale.x) / 2f;
        //Debug.Log(leftEdge + ", " + mouseRelative.x + ", " + rightEdge);

        if (mouseRelative.x > leftEdge && mouseRelative.x < rightEdge)
        {
            float distanceTo = Mathf.Infinity;

            for (int i = 0; i < options.Count; i++)
            {
                if (Vector3.Distance(options[i].transform.position, mouseOnScreen) < distanceTo)
                {
                    currentHighlight = i;
                    distanceTo = Vector3.Distance(options[i].transform.position, mouseOnScreen);
                }
            }

            HighlightOption(currentHighlight);
            selectedOption = currentHighlight;

            //highlightCircle.gameObject.SetActive(dishPickings[selectedOption].Count == 1);

            if (Input.GetButtonDown("Fire1") && !GameManager.Instance.conversationPlayer.revealingText && dishPickings[selectedOption].Count == 1)
            {
                selectingIngredient = false;
                gameManager.player.takingOrders.Add(dishPickings[selectedOption][0]);
                gameManager.conversationPlayer.ForceNextTalk();

                totalMouseMovement = Vector2.zero;
                Vector2 viewportCenter1 = ((Vector2)gameManager.foodMenu.transform.position + new Vector2(-12, -54)) / Camera.main.pixelRect.size;
                Vector2 fixedCenter1 = new Vector2(Mathf.Abs(viewportCenter1.x), Mathf.Abs(viewportCenter1.y));
                Vector2 notepadPos1 = Camera.main.ViewportToScreenPoint(fixedCenter1);
                Mouse.current.WarpCursorPosition(notepadPos1);

                foreach (Image im in options)
                {
                    im.transform.GetChild(0).gameObject.SetActive(false);
                    im.transform.GetChild(1).gameObject.SetActive(false);
                }
            }

            return;
        }

        //highlightCircle.gameObject.SetActive(true);

        foreach (Image im in GetImagesInChildren(options[selectedOption].transform.GetChild(0).gameObject))
        {
            im.color = new Color(1, 1, 1, 1f);
        }
        foreach (Image im in GetImagesInChildren(options[selectedOption].transform.GetChild(1).gameObject))
        {
            im.color = new Color(1, 1, 1, 1f);
        }

        int hover = 0;
        Vector2 input = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        totalMouseMovement += (Vector2)Camera.main.ScreenToViewportPoint(Mouse.current.position.value - lastMousePos) * Camera.main.pixelRect.size;

        switch (dishPickings[selectedOption].Count)
        {
            case 2:
                hover = 0;
                float dist0 = Vector2.Distance(options[selectedOption].transform.GetChild(0).GetChild(0).position, mouseOnScreen);

                if (Vector2.Distance(options[selectedOption].transform.GetChild(0).GetChild(1).position, mouseOnScreen) < dist0)
                {
                    hover = 1;
                    dist0 = Vector2.Distance(options[selectedOption].transform.GetChild(0).GetChild(1).position, mouseOnScreen);
                }

                highlightCircle.transform.position = options[selectedOption].transform.GetChild(0).GetChild(hover).position;
                break;

            case 3:
                hover = 0;
                float dist = Vector2.Distance(options[selectedOption].transform.GetChild(1).GetChild(0).position, mouseOnScreen);

                if (Vector2.Distance(options[selectedOption].transform.GetChild(1).GetChild(1).position, mouseOnScreen) < dist)
                {
                    hover = 1;
                    dist = Vector2.Distance(options[selectedOption].transform.GetChild(1).GetChild(1).position, mouseOnScreen);
                }

                if (Vector2.Distance(options[selectedOption].transform.GetChild(1).GetChild(2).position, mouseOnScreen) < dist)
                {
                    hover = 2;
                    dist = Vector2.Distance(options[selectedOption].transform.GetChild(1).GetChild(2).position, mouseOnScreen);
                }

                highlightCircle.transform.position = options[selectedOption].transform.GetChild(1).GetChild(hover).position;
                break;
        }

        if (Input.GetButtonDown("Fire1") && !GameManager.Instance.conversationPlayer.revealingText)
        {
            selectingIngredient = false;
            gameManager.player.takingOrders.Add(dishPickings[selectedOption][hover]);
            gameManager.conversationPlayer.ForceNextTalk();

            foreach (Image im in options)
            {
                im.transform.GetChild(0).gameObject.SetActive(false);
                im.transform.GetChild(1).gameObject.SetActive(false);
            }
        }
    }
    public void SetFoodSprites(List<Recipes> recipies)
    {
        selectingIngredient = false;

        List<Image> openOptions = options.GetRange(0, options.Count);
        List<Dish> openDishes = new List<Dish>(0);

        foreach (Recipes recipe in recipies)
        {
            if (openDishes.Contains(recipe.dishType)) continue;

            int optionIndex = UnityEngine.Random.Range(0, openOptions.Count);
            Image option = openOptions[optionIndex];
            int dishIndex = options.IndexOf(option);
            openOptions.Remove(option);
            openDishes.Add(recipe.dishType);

            dishes[dishIndex] = recipe.dishType;
            debugRecipies[dishIndex] = recipe;
            dishPickings[dishIndex].Clear();

            //Debug.Log("creating " + recipe.dishType + " at " + dishIndex);

            int index = 0;
            foreach (Ingredient op in recipe.foodOptions.options)
            {
                dishPickings[dishIndex].Add(new Recipes(recipe.dishType));
                dishPickings[dishIndex][index].foodOptions.SelectIngredient(index);

                index++;
            }

            switch (dishPickings[dishIndex].Count)
            {
                case 1:
                    option.sprite = Resources.Load<Sprite>(dishPickings[dishIndex][0].GetSpritePath());
                    break;

                case 2:
                    option.sprite = Resources.Load<Sprite>(dishPickings[dishIndex][0].GetSpritePath());
                    dishImages[dishIndex][0].sprite = Resources.Load<Sprite>(dishPickings[dishIndex][0].GetSpritePath());
                    dishImages[dishIndex][1].sprite = Resources.Load<Sprite>(dishPickings[dishIndex][1].GetSpritePath());
                    break;

                case 3:
                    option.sprite = Resources.Load<Sprite>(dishPickings[dishIndex][0].GetSpritePath());
                    dishImages[dishIndex][2].sprite = Resources.Load<Sprite>(dishPickings[dishIndex][0].GetSpritePath());
                    dishImages[dishIndex][3].sprite = Resources.Load<Sprite>(dishPickings[dishIndex][1].GetSpritePath());
                    dishImages[dishIndex][4].sprite = Resources.Load<Sprite>(dishPickings[dishIndex][2].GetSpritePath());
                    break;
            }
        }

        foreach (Image image in openOptions)
        {
            Recipes filler = Recipes.GenerateDish();

            while (openDishes.Contains(filler.dishType))
            {
                filler = Recipes.GenerateDish();
            }

            int dishIndex = options.IndexOf(image);

            openDishes.Add(filler.dishType);
            dishes[dishIndex] = filler.dishType;
            debugRecipies[dishIndex] = filler;
            dishPickings[dishIndex].Clear();

            //Debug.Log("creating " + filler.dishType + " at " + dishIndex);

            int index = 0;
            foreach (Ingredient op in filler.foodOptions.options)
            {
                dishPickings[dishIndex].Add(new Recipes(filler.dishType));
                dishPickings[dishIndex][index].foodOptions.SelectIngredient(index);

                index++;
            }

            switch (dishPickings[dishIndex].Count)
            {
                case 1:
                    image.sprite = Resources.Load<Sprite>(dishPickings[dishIndex][0].GetSpritePath());
                    break;

                case 2:
                    image.sprite = Resources.Load<Sprite>(dishPickings[dishIndex][0].GetSpritePath());
                    dishImages[dishIndex][0].sprite = Resources.Load<Sprite>(dishPickings[dishIndex][0].GetSpritePath());
                    dishImages[dishIndex][1].sprite = Resources.Load<Sprite>(dishPickings[dishIndex][1].GetSpritePath());
                    break;

                case 3:
                    image.sprite = Resources.Load<Sprite>(dishPickings[dishIndex][0].GetSpritePath());
                    dishImages[dishIndex][2].sprite = Resources.Load<Sprite>(dishPickings[dishIndex][0].GetSpritePath());
                    dishImages[dishIndex][3].sprite = Resources.Load<Sprite>(dishPickings[dishIndex][1].GetSpritePath());
                    dishImages[dishIndex][4].sprite = Resources.Load<Sprite>(dishPickings[dishIndex][2].GetSpritePath());
                    break;
            }
        }
    }

    protected override void HighlightOption(int option)
    {
        highlightCircle.transform.position = options[option].transform.position;

        foreach (Image im in options)
        {
            im.transform.GetChild(0).gameObject.SetActive(false);
            im.transform.GetChild(1).gameObject.SetActive(false);

            foreach (Image im2 in GetImagesInChildren(options[option].transform.GetChild(0).gameObject))
            {
                im2.color = new Color(1, 1, 1, 1f);
            }
            foreach (Image im2 in GetImagesInChildren(options[option].transform.GetChild(1).gameObject))
            {
                im2.color = new Color(1, 1, 1, 1f);
            }
        }

        switch (dishPickings[option].Count)
        {
            case 2:
                options[option].transform.GetChild(0).gameObject.SetActive(true);
                foreach (Image im in GetImagesInChildren(options[option].transform.GetChild(0).gameObject))
                {
                    im.color = new Color(1, 1, 1, 0.5f);
                }
                break;

            case 3:
                options[option].transform.GetChild(1).gameObject.SetActive(true);
                foreach (Image im in GetImagesInChildren(options[option].transform.GetChild(1).gameObject))
                {
                    im.color = new Color(1, 1, 1, 0.5f);
                }
                break;
        }
    }

    protected override void SelectOption(int option)
    {
        foreach (Image im2 in GetImagesInChildren(options[option].transform.GetChild(0).gameObject))
        {
            im2.color = new Color(1, 1, 1, 1f);
        }
        foreach (Image im2 in GetImagesInChildren(options[option].transform.GetChild(1).gameObject))
        {
            im2.color = new Color(1, 1, 1, 1f);
        }

        if (dishPickings[option].Count == 1)
        {
            gameManager.player.takingOrders.Add(debugRecipies[option]);
            gameManager.conversationPlayer.ForceNextTalk();

            totalMouseMovement = Vector2.zero;
            Vector2 viewportCenter1 = ((Vector2)gameManager.foodMenu.transform.position + new Vector2(-12, -54)) / Camera.main.pixelRect.size;
            Vector2 fixedCenter1 = new Vector2(Mathf.Abs(viewportCenter1.x), Mathf.Abs(viewportCenter1.y));
            Vector2 notepadPos1 = Camera.main.ViewportToScreenPoint(fixedCenter1);
            Mouse.current.WarpCursorPosition(notepadPos1);
            return;
        }

        selectedOption = option;
        selectingIngredient = true;
        mouseMovement = Vector2.zero;
        lastMousePos = Mouse.current.position.value;

        Vector2 viewportCenter = Vector2.zero;

        foreach (Image im in options)
        {
            im.transform.GetChild(0).gameObject.SetActive(false);
            im.transform.GetChild(1).gameObject.SetActive(false);
        }

        switch (dishPickings[option].Count)
        {
            case 2:
                options[option].transform.GetChild(0).gameObject.SetActive(true);
                viewportCenter = ((Vector2)(options[option].transform.GetChild(0).GetChild(0).position + options[option].transform.GetChild(0).GetChild(1).position) / 2f) / Camera.main.pixelRect.size;
                break;

            case 3:
                options[option].transform.GetChild(1).gameObject.SetActive(true);
                viewportCenter = (Vector2)options[option].transform.GetChild(1).GetChild(1).position / Camera.main.pixelRect.size;
                break;
        }

        Vector2 fixedCenter = new Vector2(Mathf.Abs(viewportCenter.x), Mathf.Abs(viewportCenter.y));
        Vector2 notepadPos = Camera.main.ViewportToScreenPoint(fixedCenter);
        Mouse.current.WarpCursorPosition(notepadPos);
        return;

        /*gameManager.player.takingOrders.Add(debugRecipies[option]);
        gameManager.conversationPlayer.ForceNextTalk();

        totalMouseMovement = Vector2.zero;
        Vector2 viewportCenter = ((Vector2)gameManager.foodMenu.transform.localPosition + new Vector2(-12, -54) - new Vector2(-960, -540)) / new Vector2(1920, 1080);
        Vector2 fixedCenter = new Vector2(Mathf.Abs(viewportCenter.x), Mathf.Abs(viewportCenter.y));
        Vector2 notepadPos = Camera.main.ViewportToScreenPoint(fixedCenter);
        Mouse.current.WarpCursorPosition(notepadPos);*/
    }

    Vector2 WalkUpPos(Transform trans)
    {
        Vector2 pos = (Vector2)trans.localPosition;

        if (trans.gameObject != gameManager.foodMenu.gameObject)
        {
            pos += WalkUpPos(trans.parent);
        }

        return pos;
    }


    List<Image> GetImagesInChildren(GameObject ob)
    {
        List<Image> ims = new List<Image>(0);

        if (ob.GetComponent<Image>() != null)
        {
            ims.Add(ob.GetComponent<Image>());
        }

        for (int i = 0; i < ob.transform.childCount; i++)
        {
            ims.AddRange(GetImagesInChildren(ob.transform.GetChild(i).gameObject));
        }

        return ims;
    }
}
