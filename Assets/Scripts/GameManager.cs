using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Utilities;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public Transform canvas;
    public Player player;
    public ConversationPlayer conversationPlayer;
    public TakingOrders takingOrders;
    public PatronSpawner patronSpawner;
    public GameObject mainCamera;
    public FoodMenu foodMenu;
    public Chef chef;
    public InputDeviceChanged playerChangedInput;
    public PlayerUpgradeManager playerUpgradeManager;
    public PlayerInputDevice lastUsedInput = PlayerInputDevice.PC;
    public bool gameIsPaused = false;
    

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        } else
        {
            Destroy(gameObject);
        }

        InputSystem.onEvent += OnPlayerInput;
        playerChangedInput?.Invoke(lastUsedInput);
        patronSpawner = Resources.FindObjectsOfTypeAll<PatronSpawner>()[0];
    }
    private void OnDestroy()
    {
        InputSystem.onEvent -= OnPlayerInput;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameIsPaused = !gameIsPaused;
            PauseGame();
        }
    }

    public void PauseGame()
    {
        if (!gameIsPaused)
        {
            Time.timeScale = 0f;
            gameIsPaused = true;
        }
        else
        {
            Time.timeScale = 1;
            gameIsPaused = false;
        }
    }

    public void OnApplicationQuit()
    {
        InputSystem.onEvent -= OnPlayerInput;
    }

    public void UpdatePlayerState(PlayerState newState)
    {
        player.StateUpdate(newState);
    }

    public void OnPlayerInput(InputEventPtr eventPtr, InputDevice device)
    {
        switch (device.displayName)
        {
            case "Keyboard":
            case "Mouse":
                lastUsedInput = PlayerInputDevice.PC;
                break;

            case "Wireless Controller":
            case "Xbox Controller":
                lastUsedInput = PlayerInputDevice.Xbox;
                break;

            case "Playstation Controller":
                lastUsedInput = PlayerInputDevice.Playstation;
                break;

            case "Switch Controller":
                lastUsedInput = PlayerInputDevice.Switch;
                break;
        }

        if (Application.isFocused)
        {
            playerChangedInput?.Invoke(lastUsedInput);
        }
    }

    public delegate void InputDeviceChanged(PlayerInputDevice newDevice);
}

public enum PlayerInputDevice
{
    PC,
    Xbox,
    Playstation,
    Switch
}
