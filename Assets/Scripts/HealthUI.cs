using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthUI : MonoBehaviour
{ 
    string emptyHeartPath = "NearyLiam_EmptyHeart_V1.0";
    GameObject heart1;
    GameObject heart2;
    GameObject heart3;
    GameObject heart4;
    GameObject heart5;
    GameObject heart6;
    GameObject flash;
    int lastHealth = 0;
    float flashing = -1;

    // Start is called before the first frame update
    public void Start()
    {
        // Heart objects
        heart1 = GameObject.Find("Canvas/Health/Heart 1");
        heart2 = GameObject.Find("Canvas/Health/Heart 2");
        heart3 = GameObject.Find("Canvas/Health/Heart 3");
        heart4 = GameObject.Find("Canvas/Health/Heart 4");
        heart5 = GameObject.Find("Canvas/Health/Heart 5");
        heart6 = GameObject.Find("Canvas/Health/Heart 6");
        flash = GameObject.Find("Canvas/Health/Flash");

        lastHealth = 3 + PlayerUpgradeManager.Instance.healthLevel;

        switch (PlayerUpgradeManager.Instance.healthLevel)
        {
            case 0:
                heart4.SetActive(false);
                heart5.SetActive(false);
                heart6.SetActive(false);
                break;
            case 1:
                heart5.SetActive(false);
                heart6.SetActive(false);
                break;
            case 2:
                heart6.SetActive(false);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        int health = GameManager.Instance.player.health;

        if (health <= 0)
        {
            SceneManager.LoadScene("End Screen");
        }

        switch (health)
        {
            case 0:
                heart1.GetComponent<Image>().sprite = Resources.Load<Sprite>(emptyHeartPath);
                break;
            case 1:
                heart2.GetComponent<Image>().sprite = Resources.Load<Sprite>(emptyHeartPath);
                break;
            case 2:
                heart3.GetComponent<Image>().sprite = Resources.Load<Sprite>(emptyHeartPath);
                break;
            case 3:
                heart4.GetComponent<Image>().sprite = Resources.Load<Sprite>(emptyHeartPath);
                break;
            case 4:
                heart5.GetComponent<Image>().sprite = Resources.Load<Sprite>(emptyHeartPath);
                break;
            case 5:
                heart6.GetComponent<Image>().sprite = Resources.Load<Sprite>(emptyHeartPath);
                break;
        }

        if (health != lastHealth)
        {
            flashing = 0;
        }

        if (flashing != -1)
        {
            flashing += Time.deltaTime * 5;

            flash.GetComponent<Image>().color = new Color(1, 0, 0, Mathf.Sin(flashing) * 0.2f);

            if (flashing >= Mathf.PI)
            {
                flashing = -1;
                flash.GetComponent<Image>().color = new Color(1, 0, 0, 0);
            }
        }

        lastHealth = health;
    }
}
