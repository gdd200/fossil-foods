using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketSpinner : Interactable
{

    public void FixedUpdate()
    {

        canInteract = (GameManager.Instance.player.takingOrders.Count != 0);
    }

    public override void Interact()
    {
        base.Interact();

        foreach (Recipes recipe in GameManager.Instance.player.takingOrders)
        {
            GameManager.Instance.chef.AddDish(recipe);
        }

        GameManager.Instance.player.takingOrders.Clear();
        GameManager.Instance.player.servedTables = 0;
    }
}
