using System;
using System.Runtime.InteropServices;
using UnityEngine;

class User32
{
    [DllImport("user32.dll")]
    public static extern bool SetCursorPos(int X, int Y);


    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    public static extern IntPtr FindWindow(string strClassName, string strWindowName);


    [DllImport("user32.dll")]
    public static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);
}