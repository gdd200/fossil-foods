using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroTalk : MonoBehaviour
{
    public ConversationPlayer conversationPlayer;
    public Conversation intro;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("PlayIntro", 2.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayIntro()
    {
        conversationPlayer.PlayConversation(intro);
        conversationPlayer.callback = new System.Action(LoadTutorial);
    }

    void LoadTutorial()
    {
        SceneManager.LoadScene("Tutorial");
    }
}
