using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Player : MonoBehaviour
{
    PlayerUpgradeManager upgrades;

    Rigidbody2D rigidbody;
    PlayerInteraction interaction;
    Animator animator;
    public List<Animator> upgradeSpites = new List<Animator>(0);
    public List<RuntimeAnimatorController> charisma = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> speed = new List<RuntimeAnimatorController>(0);

    enum MoveSprite { left, down, up, right };
    MoveSprite moveSprite = MoveSprite.right;

    GameObject camera;

    Vector2 lastVelocity = Vector2.zero;
    Vector2 velocity = Vector2.zero;

    [SerializeField]
    float acceleration = 1.0f;
    [SerializeField]
    float deceleration = 1.0f;
    [SerializeField]
    public float movementSpeed = 5.0f;

    public PlayerState state = PlayerState.Walking;

    public List<Recipes> takingOrders = new List<Recipes>();
    public List<Recipes> deliveringFood = new List<Recipes>();

    [SerializeField]
    public int health = 3;
    [SerializeField]
    public int ticketCount = 1;
    [SerializeField]
    public int traySize = 3;
    public int totalServed = 0;
    public int servedTables = 0;
    public bool hasPlate = false;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        interaction = GetComponentInChildren<PlayerInteraction>();
        animator = GetComponentInChildren<Animator>();
        animator.StartPlayback();

        camera = GameManager.Instance.mainCamera;

        upgrades = PlayerUpgradeManager.Instance;

        movementSpeed = upgrades.baseSpeed * (upgrades.speedIncrement * upgrades.speedLevel + 1f);
        acceleration = movementSpeed * 10f;
        deceleration = movementSpeed;
        ticketCount = upgrades.ticketCount + upgrades.ticketIncrement * upgrades.ticketLevel;
        traySize = upgrades.traySize + upgrades.trayIncrement * upgrades.traySizeLevel;
        health = upgrades.health;

        GetUpgradeSprites();
    }

    void GetUpgradeSprites()
    {
        for (int i = upgradeSpites.Count - 1; i >= 0; i--)
        {
            GameObject.Destroy(upgradeSpites[i].gameObject);
            upgradeSpites.RemoveAt(i);
        }

        if (upgrades.speedLevel > 0)
        {
            GameObject upgradeSprite = GameObject.Instantiate(animator.gameObject);
            upgradeSprite.GetComponent<SpriteRenderer>().sortingOrder = animator.GetComponent<SpriteRenderer>().sortingOrder + 1;
            upgradeSprite.transform.SetParent(transform);
            upgradeSprite.transform.localPosition = animator.transform.localPosition;
            upgradeSprite.transform.localRotation = animator.transform.localRotation;
            upgradeSprite.transform.localScale = animator.transform.localScale;

            Animator ani = upgradeSprite.GetComponent<Animator>();
            ani.runtimeAnimatorController = speed[upgrades.speedLevel - 1];

            upgradeSpites.Add(ani);
        }

        if (upgrades.bonusPatienceLevel > 0)
        {
            GameObject upgradeSprite = GameObject.Instantiate(animator.gameObject);
            upgradeSprite.GetComponent<SpriteRenderer>().sortingOrder = animator.GetComponent<SpriteRenderer>().sortingOrder + 2;
            upgradeSprite.transform.SetParent(transform);
            upgradeSprite.transform.localPosition = animator.transform.localPosition;
            upgradeSprite.transform.localRotation = animator.transform.localRotation;
            upgradeSprite.transform.localScale = animator.transform.localScale;

            Animator ani = upgradeSprite.GetComponent<Animator>();
            ani.runtimeAnimatorController = charisma[upgrades.bonusPatienceLevel - 1];

            upgradeSpites.Add(ani);

            if (upgrades.bonusPatienceLevel == 2)
            {
                GameObject upgradeSprite2 = GameObject.Instantiate(animator.gameObject);
                upgradeSprite2.GetComponent<SpriteRenderer>().sortingOrder = animator.GetComponent<SpriteRenderer>().sortingOrder + 2;
                upgradeSprite2.transform.SetParent(transform);
                upgradeSprite2.transform.localPosition = animator.transform.localPosition;
                upgradeSprite2.transform.localRotation = animator.transform.localRotation;
                upgradeSprite2.transform.localScale = animator.transform.localScale;

                Animator ani2 = upgradeSprite2.GetComponent<Animator>();
                ani2.runtimeAnimatorController = charisma[0];

                upgradeSpites.Add(ani2);
            }
        }
    }


    void Update()
    {
        hasPlate = (deliveringFood.Count > 0);

        switch (state)
        {
            case PlayerState.Walking:
                Movement();
                break;

            default:
                break;
        }

        /*if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            upgrades.speedLevel = Mathf.Clamp(upgrades.speedLevel - 1, 0, 3);
            GetUpgradeSprites();
        }

        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            upgrades.speedLevel = Mathf.Clamp(upgrades.speedLevel + 1, 0, 3);
            GetUpgradeSprites();
        }

        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            upgrades.bonusPatienceLevel = Mathf.Clamp(upgrades.bonusPatienceLevel - 1, 0, 3);
            GetUpgradeSprites();
        }

        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            upgrades.bonusPatienceLevel = Mathf.Clamp(upgrades.bonusPatienceLevel + 1, 0, 3);
            GetUpgradeSprites();
        }*/
    }

    private void LateUpdate()
    {
        switch (state)
        {
            case PlayerState.Walking:
                MoveCamera();
                break;

            default:
                break;
        }
    }

    public void StateUpdate(PlayerState newState)
    {
        //Debug.Log(newState);
        interaction.enabled = (newState == PlayerState.Walking);

        state = newState;
    }

    void Movement()
    {
        Vector2 inputVector = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized;

        if (inputVector.magnitude != 0)
        {
            velocity += acceleration * inputVector * Time.deltaTime;
        }
        else if (velocity.magnitude <= (velocity.normalized * deceleration).magnitude)
        {
            velocity = Vector2.zero;
        }
        else
        {
            velocity -= velocity.normalized * deceleration * Time.deltaTime;
        }

        velocity = velocity.normalized * Mathf.Clamp(velocity.magnitude, 0f, movementSpeed);

        Vector2 position = new Vector2(transform.position.x, transform.position.y);
        rigidbody.position += velocity * Time.deltaTime;

        if (velocity.magnitude > 0.01)
        {
            float angle = -Vector2.SignedAngle(Vector2.up, velocity.normalized);
            MoveSprite lastMoveSprite = (MoveSprite)(int)moveSprite;

            if (angle > 130 || angle < -130)
            {
                moveSprite = MoveSprite.up;
            } else if (angle > 50)
            {
                moveSprite = MoveSprite.right;
            } else if (angle < -50)
            {
                moveSprite = MoveSprite.left;
            } else
            {
                moveSprite = MoveSprite.down;
            }

            if (!animator.GetCurrentAnimatorStateInfo(0).IsName(moveSprite.ToString().ToLower() + (hasPlate ? "_plate" : "") + "_walk"))
            {
                animator.Play(moveSprite.ToString().ToLower() + (hasPlate ? "_plate" : "") + "_walk", -1, 0);

                foreach (Animator ani in upgradeSpites)
                {
                    ani.Play(moveSprite.ToString().ToLower() + (hasPlate ? "_plate" : "") + "_walk", -1, 0);
                }
            }

            animator.speed = velocity.magnitude / movementSpeed;

            foreach (Animator ani in upgradeSpites)
            {
                ani.speed = velocity.magnitude / movementSpeed;
            }
        } else
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName(moveSprite.ToString().ToLower() + (hasPlate ? "_plate" : "") + "_idle"))
            {
                animator.Play(moveSprite.ToString().ToLower() + (hasPlate ? "_plate" : "") + "_idle", -1, 0);

                foreach (Animator ani in upgradeSpites)
                {
                    ani.Play(moveSprite.ToString().ToLower() + (hasPlate ? "_plate" : "") + "_idle", -1, 0);
                }
            }

            animator.speed = 1f;

            foreach (Animator ani in upgradeSpites)
            {
                ani.speed = 1f;
            }
        }

        if (moveSprite == MoveSprite.left)
        {
            animator.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            animator.GetComponent<SpriteRenderer>().flipX = false;
        }

        foreach (Animator ani in upgradeSpites)
        {
            if (moveSprite == MoveSprite.left)
            {
                ani.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                ani.GetComponent<SpriteRenderer>().flipX = false;
            }
        }
    }

    void MoveCamera()
    {
        camera.transform.position = new Vector3(transform.position.x, transform.position.y, -10);
    }
}
public enum PlayerState
{
    Walking,
    TakingOrder,
    Talking,
    Serving,
    GettingFood
}