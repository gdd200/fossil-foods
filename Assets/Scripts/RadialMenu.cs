using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class RadialMenu : MonoBehaviour
{
    protected Vector2 totalMouseMovement = Vector2.zero;
    protected GameManager gameManager;
    protected int numberOfOptions = 4;
    protected float anglePerOption = 90f;
    protected float angleOffset = 0f;
    protected float deadZone = 0.1f;
    protected int currentHighlight = 0;
    protected bool handleMouseSelf = false;

    protected virtual void Start()
    {
        gameManager = GameManager.Instance;
    }

    protected void Update()
    {
        if (gameManager.lastUsedInput == PlayerInputDevice.PC && !handleMouseSelf)
        {
            Vector2 input = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
            totalMouseMovement += input;

            float angle = Vector2.SignedAngle(Vector2.up, totalMouseMovement) + angleOffset;
            if (angle > 0)
            {
                angle = 360 - angle;
            }
            angle = Mathf.Abs(angle);
            currentHighlight = Mathf.FloorToInt(angle / anglePerOption);

            HighlightOption(currentHighlight);
        } else
        {
            Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

            if (input.magnitude > deadZone)
            {
                float angle = Vector2.SignedAngle(Vector2.up, input) + angleOffset;
                if (angle > 0)
                {
                    angle = 360 - angle;
                }
                angle = Mathf.Abs(angle);
                currentHighlight = Mathf.FloorToInt(angle / anglePerOption);
                HighlightOption(currentHighlight);
            }
        }

        if (Input.GetButtonDown("Fire1"))
        {
            SelectOption(currentHighlight);
        }
    }

    protected void SetNumberOfOptions(int number)
    {
        numberOfOptions = number;
        anglePerOption = 360f / (float)numberOfOptions;


        if (numberOfOptions % 2 == 1) 
        {
            angleOffset = -anglePerOption / 2;
        }
    }

    protected virtual void HighlightOption(int option)
    {
        //Debug.Log("Highlight option: " + option);
    }

    protected virtual void SelectOption(int option)
    {
        //Debug.Log("Select option: " + option);
        totalMouseMovement = Vector2.zero;
    }
}
