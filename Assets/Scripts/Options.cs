using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Options : MonoBehaviour
{
    public GameObject main;
    public GameObject options;

    public TMP_Dropdown screenResolution;
    public TMP_Dropdown screenWindow;
    public int screenState = 0;

    public int openedTimes = 0;

    public void Start()
    {
        main.SetActive(true);
        options.SetActive(false);
        options.transform.Find("Egg").gameObject.SetActive(false);

        List<TMP_Dropdown.OptionData> ress = new List<TMP_Dropdown.OptionData>();

        foreach (Resolution res in Screen.resolutions)
        {
            //Debug.Log(res.width + "x" + res.height + ", " + res.refreshRateRatio);
            TMP_Dropdown.OptionData optionData = new TMP_Dropdown.OptionData();
            optionData.text = res.width + "x" + res.height;
            ress.Add(optionData);

            if (res.width == Screen.width && res.height == Screen.height)
            {
                ress.Insert(0, optionData);
            }
        }

        screenResolution.AddOptions(ress);

        Screen.SetResolution(Screen.width, Screen.height, false);
    }

    public void OpenOptionsMenu()
    {
        main.SetActive(false);
        options.SetActive(true);
        openedTimes++;

        if (openedTimes > 10) {
            options.transform.Find("Egg").gameObject.SetActive(true);
        }
    }

    public void CloseOptionsMenu()
    {
        main.SetActive(true);
        options.SetActive(false);
    }

    public void Gwaphics(float newVal)
    {
        QualitySettings.globalTextureMipmapLimit = Mathf.RoundToInt((1 - newVal) * 20);
        Screen.SetResolution(Mathf.RoundToInt(Mathf.Clamp(1920 * newVal, 240, 3840)), Mathf.RoundToInt(Mathf.Clamp(1020 * newVal, 135, 2160)), Screen.fullScreen);
    }

    public void SelectResolution(int selected)
    {
        Screen.SetResolution(Screen.resolutions[selected].width, Screen.resolutions[selected].height, screenState != 0);

        if (screenState == 1)
        {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
        }
        else if (screenState == 2)
        {
            Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
        }
    }

    public void SelectFullscreen(int selected)
    {
        screenState = selected;

        Screen.SetResolution(Screen.width, Screen.height, screenState != 0);

        if (screenState == 1)
        {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
        }
        else if (screenState == 2)
        {
            Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
        }
    }

    public void ChangeVolume(float volume)
    {
        AudioListener.volume = volume;
    }
}
