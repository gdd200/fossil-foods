using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientsOptions
{
    public string name;
    // True if this category must have an option for the dish to be valid
    public bool required;
    // List of Ingredient objects that are options for this category
    public List<Ingredient> options = new List<Ingredient>();
    public bool selected = false;
    public int selectedIndex = 0;

    public IngredientsOptions(string name, bool required, List<Ingredient> options)
    {
        this.name = name;
        this.required = required;
        this.options = options;
    }

    public override string ToString()
    {
        if (selected)
        {
            return options[selectedIndex].GetName();
        }
        else
        {
            return "No " + name;
        }
    }

    public string GetFileName()
    {
        if (selected)
        {
            return options[selectedIndex].GetName();
        }
        else
        {
            return "";
        }
    }

    public bool IsRequired()
    {
        return required;
    }

    public void AddOption(Ingredient ingredient)
    {
        options.Add(ingredient);
    }

    public void SelectIngredient(int index)
    {
        selectedIndex = index;
        selected = true;
    }

    public Ingredient GetSelectedIngredient()
    {
        if (selected)
        {
            return options[selectedIndex];
        }
        else
        {
            return null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

