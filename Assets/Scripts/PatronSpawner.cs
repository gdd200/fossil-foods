using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatronSpawner : MonoBehaviour
{
    public List<PatronTable> tables = new List<PatronTable>(0);
    public GameObject patronPrefab;
    public int numOfPatron = 0;
    public int count = 0;
    public float minSpawnTime = 1f;
    public float maxSpawnTime = 10f;
    public List<Patron> spawned = new List<Patron>(0);
    public bool now = false;

    void Start()
    {
        foreach (GameObject table in GameObject.FindGameObjectsWithTag("table"))
        {
            tables.Add(table.GetComponent<PatronTable>());
        }
    }

    public void DoitNow()
    {
        Invoke("SpawnPatron", 0);
        
    }

    public void SpawnPatron()
    {
        int maxOpenTableSize = MaxTableSize();
        
        if (maxOpenTableSize > 0)
        {
            int patronsInGroup = Random.Range(1, maxOpenTableSize + 1);
            PatronTable groupTable = GetOpenTableFromSize(patronsInGroup);

            List<Patron> newGroup = new List<Patron>(patronsInGroup);
            List<int> openChairs = new List<int>();

            //Debug.Log(groupTable);
            //Debug.Log(tables.Count);

            for (int i = 0; i < groupTable.seats.Count; i++)
            {
                openChairs.Add(i);
            }

            for (int i = 0; i < patronsInGroup; i++)
            {
                GameObject newPatron = Instantiate(patronPrefab, transform.position + new Vector3(Random.Range(-transform.localScale.x / 2f, transform.localScale.x / 2f), Random.Range(-transform.localScale.y / 2f, transform.localScale.y / 2f), 0), Quaternion.identity);
                Patron patron = newPatron.GetComponent<Patron>();
                spawned.Add(patron);
                now = true;
                patron.spawner = this;
                patron.leader = (i == 0);

                int chairToGoTo = openChairs[Random.Range(0, openChairs.Count)];
                patron.target = groupTable.seats[chairToGoTo];

                patron.targetTransform = groupTable.seats[chairToGoTo].transform;
                openChairs.Remove(chairToGoTo);

                newGroup.Add(patron);
            }

            for (int i = 0; i < patronsInGroup; i++)
            {
                newGroup[i].group = newGroup;
            }
        }
        count++;
        if (count >= numOfPatron)
        {
            CancelInvoke("SpawnPatron");

        }
        else 
        {
            Invoke("SpawnPatron", Random.Range(minSpawnTime, maxSpawnTime));
        }
        
    }

    int MaxTableSize()
    {
        int size = 0;

        foreach (PatronTable table in tables)
        {
            if (!table.busy && table.seats.Count > size)
            {
                size = table.seats.Count;
            }
        }

        return size;
    }

    public PatronTable GetOpenTableFromSize(int groupSize)
    {
        List<PatronTable> tablesAvailable = new List<PatronTable>(0);

        foreach (PatronTable table in tables)
        {
            if (!table.busy && groupSize <= table.seats.Count)
            {
                tablesAvailable.Add(table);
            }
        }

        if (tablesAvailable.Count == 0)
        {
            return null;
        }

        return tablesAvailable[Random.Range(0, tablesAvailable.Count)];
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "patron" && collision.GetComponent<Patron>().state == Patron.State.Leaving)
        {
            Destroy(collision.gameObject);
        }
    }
}
