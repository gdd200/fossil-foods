using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "CharacterTalk", menuName = "Fossil Foods/Character Talk", order = 2)]
public class CharacterTalk : ScriptableObject
{
    public string text = "";
    public AudioClip charSound;
    public Texture characterHead;
    public Texture acc;
    public Color color = Color.black;
    public float speed = 0.1f;
}