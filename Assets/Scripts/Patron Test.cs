using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;
using static Patron;

public class PatronTest : MonoBehaviour
{
    public GameObject asdfasdf;

    public bool spawner = false;

    public Animator animator;
    public List<Animator> details = new List<Animator>(0);
    public List<RuntimeAnimatorController> stegoColors = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> pisciColors = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> stegoHats = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> carniColors = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> carniClothes = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> raptorColors = new List<RuntimeAnimatorController>(0);
    public RuntimeAnimatorController pisciHair;
    public RuntimeAnimatorController pisciShirt;
    public RuntimeAnimatorController spinoColor;

    public bool idle = false;
    public int dino = 0;
    public int stegoColor = -1;
    public int pisciColor = -1;
    public int stegoHat = -1;
    public int pisciHairY = -1;
    public int pisciShirtY = -1;
    public int carniColor = -1;
    public int carniCloth = -1;
    public int raptorColor = -1;
    public bool isSpino = false;
    public Patron.MoveSprite moveSprite = Patron.MoveSprite.right;

    private void Start()
    {
        if (spawner)
        {
            for (int i = 0; i < stegoColors.Count; i++)
            {
                for (int j = 0; j < stegoHats.Count; j++)
                {
                    for (int k = 0; k < 8; k++)
                    {
                        GameObject pat = Instantiate(gameObject);
                        PatronTest test = pat.GetComponent<PatronTest>();
                        test.idle = Mathf.FloorToInt(k / 4) >= 1;
                        test.spawner = false;
                        test.stegoColor = i;
                        test.stegoHat = j;
                        test.moveSprite = (Patron.MoveSprite)(k % 4);
                        pat.transform.SetParent(asdfasdf.transform);
                        pat.transform.position = new Vector3(i * 2, j * 16 + k * 2, 0);
                        test.UpdateSprite();
                    }
                }
            }

            for (int i = 0; i < pisciColors.Count; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    for (int k = 0; k < 8; k++)
                    {
                        GameObject pat = Instantiate(gameObject);
                        PatronTest test = pat.GetComponent<PatronTest>();
                        test.spawner = false;
                        test.pisciColor = i;
                        test.idle = Mathf.FloorToInt(k / 4) >= 1;

                        if (j == 0)
                        {
                            test.pisciHairY = 0;
                        }

                        if (j == 1)
                        {
                            test.pisciShirtY = 0;
                        }

                        if (j == 2)
                        {
                            test.pisciHairY = 0;
                            test.pisciShirtY = 0;
                        }

                        test.moveSprite = (Patron.MoveSprite)(k % 4);
                        pat.transform.SetParent(asdfasdf.transform);
                        pat.transform.position = new Vector3(stegoColors.Count * 2 + i * 2, j * 16 + k * 2, 0);
                        test.UpdateSprite();
                    }
                }
            }
            for (int i = 0; i < carniColors.Count; i++)
            {
                for (int j = 0; j < carniClothes.Count; j++)
                {
                    for (int k = 0; k < 8; k++)
                    {
                        GameObject pat = Instantiate(gameObject);
                        PatronTest test = pat.GetComponent<PatronTest>();
                        test.idle = Mathf.FloorToInt(k / 4) >= 1;
                        test.spawner = false;
                        test.carniColor = i;
                        test.carniCloth = j;
                        test.moveSprite = (Patron.MoveSprite)(k % 4);
                        pat.transform.SetParent(asdfasdf.transform);
                        pat.transform.position = new Vector3(i * 2 + 20, j * 16 + k * 2, 0);
                        test.UpdateSprite();
                    }
                }
            }
            for (int i = 0; i < raptorColors.Count; i++)
            {
                for (int k = 0; k < 8; k++)
                {
                    GameObject pat = Instantiate(gameObject);
                    PatronTest test = pat.GetComponent<PatronTest>();
                    test.idle = Mathf.FloorToInt(k / 4) >= 1;
                    test.spawner = false;
                    test.raptorColor = i;
                    test.moveSprite = (Patron.MoveSprite)(k % 4);
                    pat.transform.SetParent(asdfasdf.transform);
                    pat.transform.position = new Vector3(i * 2 + 30, 1 * 16 + k * 2, 0);
                    test.UpdateSprite();
                }
            }
            
        }
    }

    void UpdateSprite()
    {
        animator = GetComponentInChildren<Animator>();

        if (stegoColor != -1)
        {
            dino = 0;
            animator.runtimeAnimatorController = stegoColors[stegoColor];

            if (stegoHat != -1)
            {
                GameObject hatAni = GameObject.Instantiate(animator.gameObject);
                hatAni.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
                hatAni.transform.SetParent(transform);
                hatAni.transform.localPosition = animator.transform.localPosition;
                hatAni.transform.localRotation = animator.transform.localRotation;
                hatAni.transform.localScale = animator.transform.localScale;

                Animator hat = hatAni.GetComponent<Animator>();
                hat.runtimeAnimatorController = stegoHats[stegoHat];

                details.Add(hat);
            }
        }

        if (pisciColor != -1)
        {
            dino = 1;
            animator.runtimeAnimatorController = pisciColors[pisciColor];

            if (pisciHairY != -1)
            {
                GameObject hairAni = GameObject.Instantiate(animator.gameObject);
                hairAni.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
                hairAni.transform.SetParent(transform);
                hairAni.transform.localPosition = animator.transform.localPosition;
                hairAni.transform.localRotation = animator.transform.localRotation;
                hairAni.transform.localScale = animator.transform.localScale;

                Animator hair = hairAni.GetComponent<Animator>();
                hair.runtimeAnimatorController = pisciHair;

                details.Add(hair);
            }

            if (pisciShirtY != -1)
            {
                GameObject shirtAni = GameObject.Instantiate(animator.gameObject);
                shirtAni.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
                shirtAni.transform.SetParent(transform);
                shirtAni.transform.localPosition = animator.transform.localPosition;
                shirtAni.transform.localRotation = animator.transform.localRotation;
                shirtAni.transform.localScale = animator.transform.localScale;

                Animator shirt = shirtAni.GetComponent<Animator>();
                shirt.runtimeAnimatorController = pisciShirt;

                details.Add(shirt);
            }
        }

        if (carniColor != -1)
        {
            dino = 0;
            animator.runtimeAnimatorController = carniColors[carniColor];

            if (carniCloth != -1)
            {
                GameObject hatAni = GameObject.Instantiate(animator.gameObject);
                hatAni.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
                hatAni.transform.SetParent(transform);
                hatAni.transform.localPosition = animator.transform.localPosition;
                hatAni.transform.localRotation = animator.transform.localRotation;
                hatAni.transform.localScale = animator.transform.localScale;

                Animator hat = hatAni.GetComponent<Animator>();
                hat.runtimeAnimatorController = carniClothes[carniCloth];

                details.Add(hat);
            }
        }

        if (raptorColor != -1)
        {
            dino = 0;
            animator.runtimeAnimatorController = raptorColors[raptorColor];
        }

        if (isSpino)
        {
            dino = 1;
            animator.runtimeAnimatorController = spinoColor;
        }

        //Debug.Log(moveSprite.ToString() + (idle ? " idle" : ""));
        animator.Play(moveSprite.ToString() + (idle ? " idle" : ""), -1, 0);

        if (moveSprite == MoveSprite.left && dino == 0 || moveSprite == MoveSprite.right && dino == 1)
        {
            animator.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            animator.GetComponent<SpriteRenderer>().flipX = false;
        }

        foreach (Animator detail in details)
        {
            detail.Play(moveSprite.ToString() + (idle ? " idle" : ""), -1, 0);

            if (moveSprite == MoveSprite.left && dino == 0 || moveSprite == MoveSprite.right && dino == 1)
            {
                detail.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                detail.GetComponent<SpriteRenderer>().flipX = false;
            }
        }
    }
}
