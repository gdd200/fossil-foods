using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ingredient
{
    public string name;
    public IngredientType ingredientType;

    public Ingredient(string name, IngredientType ingredientType)
    {
        this.name = name;
        this.ingredientType = ingredientType;
    }

    public string GetName()
    {
        return this.name;
    }

    public IngredientType GetIngredientType()
    {
        return ingredientType;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public enum IngredientType
{
    Meat,
    Vegetable,
    Fish
    //Gluten
}