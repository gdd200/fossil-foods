using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMenu : MonoBehaviour
{
    string checkBoxPath = "NearyLiam_CheckmarkBox_V1.0";

    public void healthButtonOnePressed()
    {
        int startHealthLevel = PlayerUpgradeManager.Instance.healthLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeHealth();
        if (PlayerUpgradeManager.Instance.healthLevel > startHealthLevel)
        {
            GameObject.Find("Canvas/Health Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Health Button 1").GetComponent<Button>().interactable = false;
            GameObject.Find("Canvas/Health Button 2").GetComponent<Button>().interactable = true;
        }
    }

    public void healthButtonTwoPressed()
    {
        int currentHealthLevel = PlayerUpgradeManager.Instance.healthLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeHealth();
        if (PlayerUpgradeManager.Instance.healthLevel > currentHealthLevel)
        {
            GameObject.Find("Canvas/Health Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Health Button 2").GetComponent<Button>().interactable = false;
            GameObject.Find("Canvas/Health Button 3").GetComponent<Button>().interactable = true;
        }
    }

    public void healthButtonThreePressed()
    {
        int currentHealthLevel = PlayerUpgradeManager.Instance.healthLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeHealth();
        if (PlayerUpgradeManager.Instance.healthLevel > currentHealthLevel)
        {
            GameObject.Find("Canvas/Health Button 3").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Health Button 3").GetComponent<Button>().interactable = false;

        }
    }

    public void charismaButtonOnePressed()
    {
        int currentPatienceLevel = PlayerUpgradeManager.Instance.bonusPatienceLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeBonusPatience();
        if (PlayerUpgradeManager.Instance.bonusPatienceLevel > currentPatienceLevel)
        {
            GameObject.Find("Canvas/Charisma Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Charisma Button 1").GetComponent<Button>().interactable = false;
            GameObject.Find("Canvas/Charisma Button 2").GetComponent<Button>().interactable = true;
        }
    }

    public void charismaButtonTwoPressed()
    {
        int currentPatienceLevel = PlayerUpgradeManager.Instance.bonusPatienceLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeBonusPatience();
        if (PlayerUpgradeManager.Instance.bonusPatienceLevel > currentPatienceLevel)
        {
            GameObject.Find("Canvas/Charisma Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Charisma Button 2").GetComponent<Button>().interactable = false;
            GameObject.Find("Canvas/Charisma Button 3").GetComponent<Button>().interactable = true;
        }
    }

    public void charismaButtonThreePressed()
    {
        int currentPatienceLevel = PlayerUpgradeManager.Instance.bonusPatienceLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeBonusPatience();
        if (PlayerUpgradeManager.Instance.bonusPatienceLevel > currentPatienceLevel)
        {
            GameObject.Find("Canvas/Charisma Button 3").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Charisma Button 3").GetComponent<Button>().interactable = false;

        }
    }

    public void speedButtonOnePressed()
    {
        int currentSpeedLevel = PlayerUpgradeManager.Instance.speedLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeSpeed();
        if (PlayerUpgradeManager.Instance.speedLevel > currentSpeedLevel)
        {
            GameObject.Find("Canvas/Speed Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Speed Button 1").GetComponent<Button>().interactable = false;
            GameObject.Find("Canvas/Speed Button 2").GetComponent<Button>().interactable = true;
        }
    }

    public void speedButtonTwoPressed()
    {
        int currentSpeedLevel = PlayerUpgradeManager.Instance.speedLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeSpeed();
        if (PlayerUpgradeManager.Instance.speedLevel > currentSpeedLevel)
        {
            GameObject.Find("Canvas/Speed Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Speed Button 2").GetComponent<Button>().interactable = false;
            GameObject.Find("Canvas/Speed Button 3").GetComponent<Button>().interactable = true;
        }
    }

    public void speedButtonThreePressed()
    {
        int currentSpeedLevel = PlayerUpgradeManager.Instance.speedLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeSpeed();
        if (PlayerUpgradeManager.Instance.speedLevel > currentSpeedLevel)
        {
            GameObject.Find("Canvas/Speed Button 3").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Speed Button 3").GetComponent<Button>().interactable = false;
        }
    }

    public void notepadButtonOnePressed()
    {
        int currentTicketLevel = PlayerUpgradeManager.Instance.ticketLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeTicketCount();
        if (PlayerUpgradeManager.Instance.ticketLevel > currentTicketLevel)
        {
            GameObject.Find("Canvas/Notepad Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Notepad Button 1").GetComponent<Button>().interactable = false;
            GameObject.Find("Canvas/Notepad Button 2").GetComponent<Button>().interactable = true;
        }
    }

    public void notepadButtonTwoPressed()
    {
        int currentTicketLevel = PlayerUpgradeManager.Instance.ticketLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeTicketCount();
        if (PlayerUpgradeManager.Instance.ticketLevel > currentTicketLevel)
        {
            GameObject.Find("Canvas/Notepad Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Notepad Button 2").GetComponent<Button>().interactable = false;
            GameObject.Find("Canvas/Notepad Button 3").GetComponent<Button>().interactable = true;
        }
    }

    public void notepadButtonThreePressed()
    {
        int currentTicketLevel = PlayerUpgradeManager.Instance.ticketLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeTicketCount();
        if (PlayerUpgradeManager.Instance.ticketLevel > currentTicketLevel)
        {
            GameObject.Find("Canvas/Notepad Button 3").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Notepad Button 3").GetComponent<Button>().interactable = false;
        }
    }

    public void traysizeButtonOnePressed()
    {
        int currentTrayLevel = PlayerUpgradeManager.Instance.traySizeLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeTraySize();
        if (PlayerUpgradeManager.Instance.traySizeLevel > currentTrayLevel)
        {
            GameObject.Find("Canvas/Tray Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Tray Button 1").GetComponent<Button>().interactable = false;
            GameObject.Find("Canvas/Tray Button 2").GetComponent<Button>().interactable = true;
        }
    }

    public void traysizeButtonTwoPressed()
    {
        int currentTrayLevel = PlayerUpgradeManager.Instance.traySizeLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeTraySize();
        if (PlayerUpgradeManager.Instance.traySizeLevel > currentTrayLevel)
        {
            GameObject.Find("Canvas/Tray Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Tray Button 2").GetComponent<Button>().interactable = false;
            GameObject.Find("Canvas/Tray Button 3").GetComponent<Button>().interactable = true;
        }
    }

    public void traysizeButtonThreePressed()
    {
        int currentTrayLevel = PlayerUpgradeManager.Instance.traySizeLevel;
        // Check if it actually upgraded, if so check the box
        PlayerUpgradeManager.Instance.upgradeTraySize();
        if (PlayerUpgradeManager.Instance.traySizeLevel > currentTrayLevel)
        {
            GameObject.Find("Canvas/Tray Button 3").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
            GameObject.Find("Canvas/Tray Button 3").GetComponent<Button>().interactable = false;
        }
    }

    void Update()
    {
        TextMeshProUGUI currentMoneyText = GameObject.Find("Canvas/Current Money").GetComponent<TextMeshProUGUI>();
        currentMoneyText.text = PlayerUpgradeManager.Instance.playerWallet.ToString();
    }

    private void Start()
    {
        // Sets menu to remember previous upgrades
        switch (PlayerUpgradeManager.Instance.healthLevel)
        {
            case 1:
                GameObject.Find("Canvas/Health Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Health Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Health Button 2").GetComponent<Button>().interactable = true;
                break;
            case 2:
                GameObject.Find("Canvas/Health Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Health Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Health Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Health Button 2").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Health Button 3").GetComponent<Button>().interactable = true;
                break;
            case 3:
                GameObject.Find("Canvas/Health Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Health Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Health Button 3").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Health Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Health Button 2").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Health Button 3").GetComponent<Button>().interactable = false;
                break;
        }
        switch (PlayerUpgradeManager.Instance.bonusPatienceLevel)
        {
            case 1:
                GameObject.Find("Canvas/Charisma Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Charisma Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Charisma Button 2").GetComponent<Button>().interactable = true;
                break;
            case 2:
                GameObject.Find("Canvas/Charisma Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Charisma Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Charisma Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Charisma Button 2").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Charisma Button 3").GetComponent<Button>().interactable = true;
                break;
            case 3:
                GameObject.Find("Canvas/Charisma Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Charisma Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Charisma Button 3").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Charisma Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Charisma Button 2").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Charisma Button 3").GetComponent<Button>().interactable = false;
                break;
        }
        switch (PlayerUpgradeManager.Instance.speedLevel)
        {
            case 1:
                GameObject.Find("Canvas/Speed Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Speed Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Speed Button 2").GetComponent<Button>().interactable = true;
                break;
            case 2:
                GameObject.Find("Canvas/Speed Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Speed Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Speed Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Speed Button 2").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Speed Button 3").GetComponent<Button>().interactable = true;
                break;
            case 3:
                GameObject.Find("Canvas/Speed Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Speed Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Speed Button 3").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Speed Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Speed Button 2").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Speed Button 3").GetComponent<Button>().interactable = false;
                break;
        }
        switch (PlayerUpgradeManager.Instance.ticketLevel)
        {
            case 1:
                GameObject.Find("Canvas/Notepad Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Notepad Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Notepad Button 2").GetComponent<Button>().interactable = true;
                break;
            case 2:
                GameObject.Find("Canvas/Notepad Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Notepad Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Notepad Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Notepad Button 2").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Notepad Button 3").GetComponent<Button>().interactable = true;
                break;
            case 3:
                GameObject.Find("Canvas/Notepad Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Notepad Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Notepad Button 3").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Notepad Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Notepad Button 2").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Notepad Button 3").GetComponent<Button>().interactable = false;
                break;
        }
        switch (PlayerUpgradeManager.Instance.traySizeLevel)
        {
            case 1:
                GameObject.Find("Canvas/Tray Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Tray Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Tray Button 2").GetComponent<Button>().interactable = true;
                break;
            case 2:
                GameObject.Find("Canvas/Tray Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Tray Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Tray Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Tray Button 2").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Tray Button 3").GetComponent<Button>().interactable = true;
                break;
            case 3:
                GameObject.Find("Canvas/Tray Button 1").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Tray Button 2").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Tray Button 3").GetComponent<Image>().sprite = Resources.Load<Sprite>(checkBoxPath);
                GameObject.Find("Canvas/Tray Button 1").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Tray Button 2").GetComponent<Button>().interactable = false;
                GameObject.Find("Canvas/Tray Button 3").GetComponent<Button>().interactable = false;
                break;
        }
    }
}
