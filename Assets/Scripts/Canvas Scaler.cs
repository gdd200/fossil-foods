using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasScaler : MonoBehaviour
{
    void Update()
    {
        Vector2 scale = Camera.main.pixelRect.size / new Vector2(1920, 1080);
        GetComponent<Canvas>().scaleFactor = Mathf.Min(scale.x, scale.y);
    }
}
