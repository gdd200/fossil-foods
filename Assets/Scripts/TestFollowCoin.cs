using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestFollowCoin : MonoBehaviour
{
    RectTransform rect;

    void Start()
    {
        rect = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        rect.position = Camera.main.WorldToScreenPoint(GameManager.Instance.player.transform.position + new Vector3(0, 1));
    }
}
