using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Conversation", menuName = "Fossil Foods/Conversation", order = 1)]
public class Conversation : ScriptableObject
{
    public List<CharacterTalk> characterTalks = new List<CharacterTalk>();
    public bool skippable = true;
}