using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Recipes
{
    public Dish dishType;
    public string name;
    public IngredientsOptions foodOptions;
    public PatronTable origin;

    // Constructor that takes a dish type and list of ingredient options
    public Recipes(Dish dishType)
    {
        this.dishType = dishType;
        CreateOptions();
    }

    // Prints out the name followed by all the ingredients
    public override string ToString()
    {
        if (foodOptions.options.Count > 1)
        {
            string dishDescription = name + " with " + foodOptions.ToString();
            return dishDescription;
        }
        else
        {
            return name;
        }
    }

    public string GetSpriteName()
    {
        string dishDescription = dishType.ToString() + foodOptions.ToString();
        return dishDescription;
    }

    // Overload == operator to check for the same dish and selected ingredients
    public static bool operator ==(Recipes a, Recipes b)
    {
        //bool equals = a.GetSpritePath().Equals(b.GetSpritePath()); 
        //Debug.Log(equals);
        //return equals;
        bool equals = true;
        // Check that it's the same dish type
        if (a.dishType == b.dishType)
        {
            // Check that each selected ingredient option is the same
            if (a.foodOptions.selectedIndex != b.foodOptions.selectedIndex)
            {
                equals = false;
            }
        }
        else
        {
            equals = false;
        }
        return equals;
    }

    public override bool Equals(object b)
    {
        return (this == (Recipes)b);
    }

    public bool Equals(Recipes b)
    {
        return (this == b);
    }

    // Overload != operator
    public static bool operator !=(Recipes a, Recipes b)
    {
        return !(a == b);
    }

    // Create the ingredients and options available depending on the food
    public void CreateOptions()
    {
        if (dishType == Dish.Soup)
        {
            name = "Soup";
            // All potential ingredients for egg soup
            Ingredient egg = new Ingredient("Egg", IngredientType.Meat);
            Ingredient vegetables = new Ingredient("Algae", IngredientType.Vegetable);
            // Options for soup type
            List<Ingredient> soupList = new List<Ingredient> { egg, vegetables };
            foodOptions = new IngredientsOptions("Egg", true, soupList);
        }
        else if (dishType == Dish.PrehistoricPizza)
        {
            name = "Prehistoric Pizza";
            // All potential ingredients for prehistoric pizza
            Ingredient meat = new Ingredient("Meat", IngredientType.Meat);
            Ingredient fish = new Ingredient("Fish", IngredientType.Fish);
            Ingredient vegetables = new Ingredient("Vegetables", IngredientType.Vegetable);
            // Options for toppings
            List<Ingredient> toppingList = new List<Ingredient> { meat, fish, vegetables };
            foodOptions = new IngredientsOptions("Toppings", true, toppingList);
        }
        else if (dishType == Dish.GiantStick)
        {
            name = "Giant Stick";
            // All potential ingredients for giant centipede on a Stick
            Ingredient stick = new Ingredient("Leaves", IngredientType.Vegetable);
            Ingredient centipede = new Ingredient("Centipede", IngredientType.Meat);
            // Options for the dish
            List<Ingredient> centipedeList = new List<Ingredient> { stick, centipede };
            foodOptions = new IngredientsOptions("Centipede and Leaves", true, centipedeList);
        }
        else if (dishType == Dish.Sushi)
        {
            name = "Sushi";
            // All potential ingredients
            Ingredient fish = new Ingredient("Fish", IngredientType.Fish);
            Ingredient seaweed = new Ingredient("Seaweed", IngredientType.Vegetable);
            Ingredient meat = new Ingredient("Meat", IngredientType.Meat);
            // Options for the dish
            List<Ingredient> sushiList = new List<Ingredient> { fish, seaweed, meat };
            foodOptions = new IngredientsOptions("Fish and Seaweed", true, sushiList);
        }
        else if (dishType == Dish.Trilobites)
        {
            name = "Trilobites";
            // All potential ingredients
            Ingredient trilobite = new Ingredient("Trilobite", IngredientType.Fish);
            // Options for the dish
            List<Ingredient> trilobiteList = new List<Ingredient> { trilobite };
            foodOptions = new IngredientsOptions("Trilobite", true, trilobiteList);
        }
        else if (dishType == Dish.CavemanIcepop)
        {
            name = "Caveman Icepop";
            // All potential ingredients
            Ingredient caveman = new Ingredient("Caveman", IngredientType.Meat);
            // Options for the dish
            List<Ingredient> cavemanList = new List<Ingredient> { caveman };
            foodOptions = new IngredientsOptions("Caveman", true, cavemanList);
        }
        else if (dishType == Dish.TreeFernSalad)
        {
            name = "Tree Fern Salad";
            // All potential ingredients
            Ingredient treeFern = new Ingredient("Tree Fern", IngredientType.Vegetable);
            // Options for the dish
            List<Ingredient> treeFernList = new List<Ingredient> { treeFern };
            foodOptions = new IngredientsOptions("Tree Fern", true, treeFernList);
        }
        //else if (dishType == Dish.AlgaeSoup)
        //{
        //    name = "Algae Soup";
        //    // All potential ingredients
        //    Ingredient algae = new Ingredient("Algae", IngredientType.Vegetable);
        //    // Options for the dish
        //    List<Ingredient> algaeList = new List<Ingredient> { algae };
        //    foodOptions = new IngredientsOptions("Algae", true, algaeList);
        //}
        else if (dishType == Dish.Nuggets)
        {
            name = "Nuggets";
            // All potential ingredients
            Ingredient dodo = new Ingredient("Dodo", IngredientType.Meat);
            Ingredient megalodon = new Ingredient("Megalodon", IngredientType.Fish);
            // Options for the dish
            List<Ingredient> meatList = new List<Ingredient> { dodo, megalodon };
            foodOptions = new IngredientsOptions("Meat", true, meatList);
        }
        else if (dishType == Dish.Burger)
        {
            name = "Burger";
            // All potential ingredients
            Ingredient giantSloth = new Ingredient("Giant Sloth", IngredientType.Meat);
            Ingredient vegetable = new Ingredient("Vegetable", IngredientType.Vegetable);
            // Options for the dish
            List<Ingredient> slothBurgerList = new List<Ingredient> { giantSloth, vegetable };
            foodOptions = new IngredientsOptions("Toppings", true, slothBurgerList);
        }
    }

    // Get the correct sprite for the selected options
    public string GetSpritePath()
    {
        string filePath = "Foods/" + GetSpriteName();
        return filePath;
    }

    // Get the correct detailed sprite for the selected options
    public string GetDetailedSpritePath()
    {
        string filePath = "DetailedFoods/" + GetSpriteName();

        if (Resources.Load<Sprite>(filePath) == null)
        {
            filePath = "Foods/" + GetSpriteName();
        }

        return filePath;
    }

    public string GetEatPath()
    {
        string filePath = "EatFoods/" + GetSpriteName();
        return filePath;
    }

    // Start is called before the first frame update
    void Start()
    {
        /* Used for testing */
        //dishType = Dish.PrehistoricPizza;
        //CreateOptions();

        //ingredientsOptions[1].SelectIngredient(2);
        //ingredientsOptions[2].SelectIngredient(0);
        //Debug.Log(GetSpritePath());
    }

    public static Recipes GenerateDish()
    {
        int numberOfDishes = Enum.GetNames(typeof(Dish)).Length;
        Dish dishType = (Dish)UnityEngine.Random.Range(0, numberOfDishes);
        Recipes order = new Recipes(dishType);
        order.foodOptions.SelectIngredient(UnityEngine.Random.Range(0, order.foodOptions.options.Count()));
        return order;
    }

    public static Recipes GenerateDish(Patron.Diet diet)
    {
        //Debug.Log(diet);
        int numberOfDishes = Enum.GetNames(typeof(Dish)).Length;
        Dish dishType;
        Recipes order;
        IngredientType neededType;
        IngredientType secondType;
        switch (diet)
        {
            case Patron.Diet.Vegetarian:
                dishType = (Dish)UnityEngine.Random.Range(0, 6);
                order = new Recipes(dishType);
                neededType = IngredientType.Vegetable;
                secondType = neededType;
                break;
            case Patron.Diet.Piscivore:
                dishType = (Dish)UnityEngine.Random.Range(4, 8);
                order = new Recipes(dishType);
                neededType = IngredientType.Fish;
                secondType = neededType;
                break;
            default:
                dishType = (Dish)UnityEngine.Random.Range(1, numberOfDishes);
                order = new Recipes(dishType);
                neededType = IngredientType.Meat;
                secondType = IngredientType.Fish;
                break;
        }

        //else // Fallback just in case even though it'll probably just break anyway.
        //{
        //    dishType = (Dish)UnityEngine.Random.Range(0, numberOfDishes);
        //    order = new Recipes(dishType);
        //    neededType = IngredientType.Meat;
        //    secondType = neededType;
        //}
        bool properIngredients = false;
        while (!properIngredients)
        {
            order.foodOptions.SelectIngredient(UnityEngine.Random.Range(0, order.foodOptions.options.Count()));
            if (order.foodOptions.GetSelectedIngredient().ingredientType == neededType)
            {
                properIngredients = true;
            }
            else if (order.foodOptions.GetSelectedIngredient().ingredientType == secondType)
            {
                properIngredients = true;
            }
        }
        return order;
    }


    // Update is called once per frame
    void Update()
    {
        //    int numberOfDishes = Enum.GetNames(typeof(Dish)).Length;
        //    Dish dishType = (Dish)UnityEngine.Random.Range(0, numberOfDishes);
        //    Recipes order = new Recipes(dishType);
        //    foreach (IngredientsOptions options in order.ingredientsOptions)
        //    {
        //        options.SelectIngredient(UnityEngine.Random.Range(0, options.options.Count));
        //    }
        //    return order;
        //}
    }
}

public enum Dish
{
    TreeFernSalad,
    Soup,
    GiantStick,
    Burger,
    Sushi,
    PrehistoricPizza,
    Trilobites,
    Nuggets,
    CavemanIcepop,
}