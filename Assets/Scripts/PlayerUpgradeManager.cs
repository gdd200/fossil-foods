using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerUpgradeManager : MonoBehaviour
{
    // Singleton so that there's only one upgrade manager
    public static PlayerUpgradeManager Instance;

    GameManager gameManager = GameManager.Instance;

    public static bool gameIsPaused;
    public static string upgradeScene = "Liam_Upgrades_Scene";
    public static string restaurantScene = "Restaurant";

    // Player money
    public int playerWallet = 0;

    // Upgrade costs per level
    public int cost1 = 10;
    public int cost2 = 25;
    public int cost3 = 50;

    // Player stats and base values
    public float speed = 5.0f;
    public float baseSpeed = 5.0f;
    public float speedIncrement = 0.25f;
    public float speedMultiplier = 1.0f;
    public int speedLevel = 0;

    public int traySize = 3;
    public int trayIncrement = 1;
    public int traySizeLevel = 0;

    public int health = 3;
    public int healthIncrement = 1;
    public int healthLevel = 0;

    public float bonusPatience = 0.0f;
    public float patienceIncrement = 10.0f;
    public int bonusPatienceLevel = 0;

    public int ticketCount = 1;
    public int ticketIncrement = 1;
    public int ticketLevel = 0;

    public int dayCount = 0;

    public void upgradeSpeed()
    {
        bool canAfford = false;
        switch (speedLevel)
        {
            case 0:
                if (playerWallet - cost1 >= 0)
                {
                    playerWallet -= cost1;
                    canAfford = true;
                }
                break;
            case 1:
                if (playerWallet - cost2 >= 0)
                {
                    playerWallet -= cost2;
                    canAfford = true;
                }
                break;
            case 2:
                if (playerWallet - cost3 >= 0)
                {
                    playerWallet -= cost3;
                    canAfford = true;
                }
                break;
        }
        if (canAfford)
        {
            speedMultiplier += speedIncrement;
            speed = baseSpeed * speedMultiplier;
            //GameManager.Instance.player.SetMovementSpeed(speed);
            speedLevel++;
        }
    }

    public void upgradeTraySize()
    {
        bool canAfford = false;
        switch (traySizeLevel)
        {
            case 0:
                if (playerWallet - cost1 >= 0)
                {
                    playerWallet -= cost1;
                    canAfford = true;
                }
                break;
            case 1:
                if (playerWallet - cost2 >= 0)
                {
                    playerWallet -= cost2;
                    canAfford = true;
                }
                break;
            case 2:
                if (playerWallet - cost3 >= 0)
                {
                    playerWallet -= cost3;
                    canAfford = true;
                }
                break;
        }
        if (canAfford)
        {
            traySize += trayIncrement;
            traySizeLevel++;
        }
    }

    public void upgradeHealth()
    {
        bool canAfford = false;
        switch (healthLevel)
        {
            case 0:
                if (playerWallet - cost1 >= 0)
                {
                    playerWallet -= cost1;
                    canAfford = true;
                }
                break;
            case 1:
                if (playerWallet - cost2 >= 0)
                {
                    playerWallet -= cost2;
                    canAfford = true;
                }
                break;
            case 2:
                if (playerWallet - cost3 >= 0)
                {
                    playerWallet -= cost3;
                    canAfford = true;
                }
                break;
        }
        if (canAfford)
        {
            health += healthIncrement;
            healthLevel++;
        }
    }

    public void upgradeBonusPatience()
    {
        bool canAfford = false;
        switch (bonusPatienceLevel)
        {
            case 0:
                if (playerWallet - cost1 >= 0)
                {
                    playerWallet -= cost1;
                    canAfford = true;
                }
                break;
            case 1:
                if (playerWallet - cost2 >= 0)
                {
                    playerWallet -= cost2;
                    canAfford = true;
                }
                break;
            case 2:
                if (playerWallet - cost3 >= 0)
                {
                    playerWallet -= cost3;
                    canAfford = true;
                }
                break;
        }
        if (canAfford)
        {
            bonusPatience += patienceIncrement;
            bonusPatienceLevel++;
        }
    }

    public void upgradeTicketCount()
    {
        bool canAfford = false;
        switch (ticketLevel)
        {
            case 0:
                if (playerWallet - cost1 >= 0)
                {
                    playerWallet -= cost1;
                    canAfford = true;
                }
                break;
            case 1:
                if (playerWallet - cost2 >= 0)
                {
                    playerWallet -= cost2;
                    canAfford = true;
                }
                break;
            case 2:
                if (playerWallet - cost3 >= 0)
                {
                    playerWallet -= cost3;
                    canAfford = true;
                }
                break;
        }
        if (canAfford)
        {
            ticketCount += ticketIncrement;
            ticketLevel++;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Ensure singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            gameIsPaused = !gameIsPaused;
            PauseGame();
        }
    }

    void PauseGame()
    {
        if (gameIsPaused)
        {
            Time.timeScale = 0f;
            SceneManager.LoadScene(upgradeScene);
        }
        else
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(restaurantScene);
        }
    }
}
