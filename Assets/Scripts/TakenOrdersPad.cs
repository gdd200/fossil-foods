using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TakenOrdersPad : MonoBehaviour
{
    public List<Image> orderImages = new List<Image>(0);

    public float fadeTimer = 0f;
    public int fadeMode = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for (int i = 0; i < Mathf.Min(GameManager.Instance.player.takingOrders.Count, 8); i++)
        {
            orderImages[i].enabled = true;
            orderImages[i].sprite = Resources.Load<Sprite>(GameManager.Instance.player.takingOrders[i].GetSpritePath());
        }

        for (int i = GameManager.Instance.player.takingOrders.Count; i < 8; i++)
        {
            orderImages[i].enabled = false;
        }
    }

    private void Update()
    {
        if (GameManager.Instance.player.takingOrders.Count <= 4)
        {
            fadeMode = -1;

            orderImages[0].color = Color.white;
            orderImages[1].color = Color.white;
            orderImages[2].color = Color.white;
            orderImages[3].color = Color.white;
            orderImages[4].color = Color.clear;
            orderImages[5].color = Color.clear;
            orderImages[6].color = Color.clear;
            orderImages[7].color = Color.clear;
        } else if (fadeMode == -1)
        {
            fadeMode = 0;
            fadeTimer = 0f;
        }

        if (fadeMode != -1)
        {
            fadeTimer += Time.deltaTime;

            switch (fadeMode)
            {
                case 0:
                    {
                        orderImages[0].color = Color.white;
                        orderImages[1].color = Color.white;
                        orderImages[2].color = Color.white;
                        orderImages[3].color = Color.white;
                        orderImages[4].color = Color.clear;
                        orderImages[5].color = Color.clear;
                        orderImages[6].color = Color.clear;
                        orderImages[7].color = Color.clear;

                        if (fadeTimer > 3f)
                        {
                            fadeTimer = 0;
                            fadeMode = 1;
                        }
                    }
                    break;

                case 1:
                    {
                        orderImages[0].color = new Color(1, 1, 1, 1f - fadeTimer);
                        orderImages[1].color = new Color(1, 1, 1, 1f - fadeTimer);
                        orderImages[2].color = new Color(1, 1, 1, 1f - fadeTimer);
                        orderImages[3].color = new Color(1, 1, 1, 1f - fadeTimer);
                        orderImages[4].color = new Color(1, 1, 1, fadeTimer);
                        orderImages[5].color = new Color(1, 1, 1, fadeTimer);
                        orderImages[6].color = new Color(1, 1, 1, fadeTimer);
                        orderImages[7].color = new Color(1, 1, 1, fadeTimer);

                        if (fadeTimer > 1f)
                        {
                            fadeTimer = 0;
                            fadeMode = 2;
                        }
                    }
                    break;

                case 2:
                    {
                        orderImages[0].color = Color.clear;
                        orderImages[1].color = Color.clear;
                        orderImages[2].color = Color.clear;
                        orderImages[3].color = Color.clear;
                        orderImages[4].color = Color.white;
                        orderImages[5].color = Color.white;
                        orderImages[6].color = Color.white;
                        orderImages[7].color = Color.white;

                        if (fadeTimer > 3f)
                        {
                            fadeTimer = 0;
                            fadeMode = 3;
                        }
                    }
                    break;

                case 3:
                    {
                        orderImages[0].color = new Color(1, 1, 1, fadeTimer);
                        orderImages[1].color = new Color(1, 1, 1, fadeTimer);
                        orderImages[2].color = new Color(1, 1, 1, fadeTimer);
                        orderImages[3].color = new Color(1, 1, 1, fadeTimer);
                        orderImages[4].color = new Color(1, 1, 1, 1f - fadeTimer);
                        orderImages[5].color = new Color(1, 1, 1, 1f - fadeTimer);
                        orderImages[6].color = new Color(1, 1, 1, 1f - fadeTimer);
                        orderImages[7].color = new Color(1, 1, 1, 1f - fadeTimer);

                        if (fadeTimer > 1f)
                        {
                            fadeTimer = 0;
                            fadeMode = 0;
                        }
                    }
                    break;
            }
        }
    }
}
