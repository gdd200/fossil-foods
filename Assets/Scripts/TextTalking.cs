using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTalking : Interactable
{
    public Conversation conversation;

    public override void Interact()
    {
        //Debug.Log("Interaction with " + gameObject.name);
        gameManager.UpdatePlayerState(PlayerState.Talking);
        gameManager.conversationPlayer.PlayConversation(conversation);
    }
}
