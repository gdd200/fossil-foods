using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public List<Interactable> interactables;
    Interactable closest;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.player.state != PlayerState.Walking) return;

        if (interactables.Count <= 0)
        {
            closest = null;
            return;
        }

        if (closest == null)
        {
            closest = GetClosest();

            if (closest != null)
            {
                closest.popup.SetActive(true);
            }
        }

        if (closest != GetClosest())
        {
            closest.popup.SetActive(false);

            closest = GetClosest();

            if (closest != null)
            {
                closest.popup.SetActive(true);
            }
        }

        if (Input.GetButtonDown("Fire1") && !GameManager.Instance.gameIsPaused && closest != null && !GameManager.Instance.conversationPlayer.gameObject.activeSelf)
        {
            GetClosest().Interact();
        }
    }

    Interactable GetClosest()
    {
        Interactable closestInteraction = interactables[0];

        for (int i = 1; i < interactables.Count; i++)
        {
            if (interactables[i].canInteract && Vector3.Distance(interactables[i].transform.position, transform.position) < Vector3.Distance(closestInteraction.transform.position, transform.position))
            {
                closestInteraction = interactables[i];
            }
        }

        return (closestInteraction.canInteract ? closestInteraction : null);
    }
}
