using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoveTextToTip : MonoBehaviour
{
    Camera camera;
    GameObject dinoDollar;

    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
        dinoDollar = GameObject.Find("Dino Dollar");
    }

    // Update is called once per frame
    void Update()
    {
        if (dinoDollar != null)
        {
            gameObject.transform.position = camera.WorldToScreenPoint(dinoDollar.gameObject.transform.position + Vector3.up + Vector3.right);
        }
    }
}
