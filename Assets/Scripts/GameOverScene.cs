using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameOverScene : MonoBehaviour
{
    public Image spotlight;
    public RectTransform diloIsDeadKekw;
    public TMP_Text gameOverText;
    public Image buttonCover;
    public AudioSource pipe;

    public AudioClip endSound;

    public float fadeIn = 0;


    public void Start()
    {
        spotlight.color = new Color(1, 1, 1, 0);
        diloIsDeadKekw.gameObject.SetActive(false);
        gameOverText.color = new Color(0.7686275f, 0.172549f, 0.2117647f, 0);
        buttonCover.gameObject.SetActive(true);
        buttonCover.color = new Color(buttonCover.color.r, buttonCover.color.g, buttonCover.color.b, 1);

        StartCoroutine(DoGameOver());
    }

    IEnumerator DoGameOver()
    {
        while (fadeIn <= 2)
        {
            fadeIn += Time.deltaTime;
            gameOverText.color = new Color(0.7686275f, 0.172549f, 0.2117647f, fadeIn / 2f);

            yield return null;
        }

        gameOverText.color = new Color(0.7686275f, 0.172549f, 0.2117647f, 1);
        fadeIn = 0;

        while (fadeIn <= 2)
        {
            fadeIn += Time.deltaTime;

            spotlight.color = new Color(1, 1, 1, fadeIn / 2f);

            yield return null;
        }

        spotlight.color = new Color(1, 1, 1, 1);
        fadeIn = 0;

        diloIsDeadKekw.localPosition = new Vector3(0, 10000, 0);
        diloIsDeadKekw.gameObject.SetActive(true);

        while (fadeIn <= 1)
        {
            fadeIn += Time.deltaTime;

            diloIsDeadKekw.localPosition = new Vector3(0, Mathf.Lerp(10000, 0, fadeIn), 0);

            yield return null;
        }

        fadeIn = 0;
        diloIsDeadKekw.localPosition = new Vector3(0, 0, 0);
        pipe.Play();

        while (pipe.isPlaying)
        {
            yield return null;
        }

        pipe.clip = endSound;
        pipe.Play();

        while (fadeIn <= 1)
        {
            fadeIn += Time.deltaTime;

            buttonCover.color = new Color(buttonCover.color.r, buttonCover.color.g, buttonCover.color.b, 1f - fadeIn);

            yield return null;
        }

        buttonCover.color = new Color(buttonCover.color.r, buttonCover.color.g, buttonCover.color.b, 1);
        buttonCover.gameObject.SetActive(false);
    }

    public void TitleScreenPressed() 
    {
        SceneManager.LoadScene("Title Scene");
    }

    public void Retry()
    {
        SceneManager.LoadScene("Restaurant");
    }
}
