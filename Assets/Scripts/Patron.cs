using Pathfinding;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.Animations;
using System.Xml.Schema;
using TMPro;
using System.Diagnostics.CodeAnalysis;
using UnityEngine.UI;

public class Patron : MonoBehaviour
{
    public List<RuntimeAnimatorController> stegoColors = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> pisciColors = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> carniColors = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> carniClothes = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> stegoHats = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> raptorColors = new List<RuntimeAnimatorController>(0);
    public List<RuntimeAnimatorController> killerColors = new List<RuntimeAnimatorController>(0);
    public RuntimeAnimatorController pisciHair;
    public RuntimeAnimatorController pisciShirt;
    public RuntimeAnimatorController spinoColor;
    public RuntimeAnimatorController karen;
    public int dino = 0;

    public string name = "something broke lol";
    public Recipes order;
    public Diet diet;
    public State state = State.GoingToTable;
    public enum MoveSprite { left, down, up, right };
    public MoveSprite moveSprite = MoveSprite.right;
    public MoveSprite lastMoveSprite;

    public CharacterTalk orderTalk;
    public string headPath = "Profiles/";
    public string accPath = null;
    public Texture head;
    public Texture acc;
    public AudioClip talkingClip;
    public Animator animator;
    public List<Animator> details = new List<Animator>(0);
    public float movementSpeed = 3;
    public bool flipped = false;

    public float maxPatience = 180;
    public float patience = 1;
    public PatienceBar patienceBar;

    public GameObject dinoDollar;
    public GameObject tipText;
    public bool hasTipped = false;
    public AudioSource patronAudio;
    public AudioClip tipAudio;

    public List<Patron> group = new List<Patron>(0);

    // AI PATHING
    public bool leader = false;
    public PatronSpawner spawner;
    IAstarAI ai;
    public PatronSeat target;
    public PatronTable table;
    public Transform targetTransform;
    public Vector2 lastPosition = Vector2.zero;

    public bool logVel = false;

    public GameObject debugCircle;

    void Start()
    {
        ai = GetComponent<IAstarAI>();
        lastPosition = transform.position;

        name = "Test name";

        patience = maxPatience + PlayerUpgradeManager.Instance.bonusPatience;
        patienceBar = GetComponentInChildren<PatienceBar>();
        patienceBar.gameObject.SetActive(false);
        animator = GetComponentInChildren<Animator>();
        dinoDollar.SetActive(false);

        int whatDino = Random.Range(0, 7);
        //Debug.Log("what dino:" + whatDino);
        if (whatDino == 0)
        {
            dino = 0;
            int stegoColor = Random.Range(0, stegoColors.Count - 1);
            animator.runtimeAnimatorController = stegoColors[stegoColor];
            diet = Diet.Vegetarian;
            headPath += "Stego" + stegoColor;

            if (Random.Range(0f, 1f) > 0.5f)
            {
                GameObject hatAni = GameObject.Instantiate(animator.gameObject);
                hatAni.GetComponent<SpriteRenderer>().sortingOrder = animator.GetComponent<SpriteRenderer>().sortingOrder + 1;
                hatAni.transform.SetParent(transform);
                hatAni.transform.localPosition = animator.transform.localPosition;
                hatAni.transform.localRotation = animator.transform.localRotation;
                hatAni.transform.localScale = animator.transform.localScale;

                Animator hat = hatAni.GetComponent<Animator>();
                int hatNum = Random.Range(0, stegoHats.Count - 1);
                hat.runtimeAnimatorController = stegoHats[hatNum];
                accPath = "Profiles/StegoHat" + hatNum;

                details.Add(hat);
            }
        } 
        else if (whatDino == 1)
        {
            dino = 1;
            int pisciColor = Random.Range(0, pisciColors.Count - 1);
            animator.runtimeAnimatorController = pisciColors[pisciColor];
            headPath += "Pisci" + pisciColor;
            diet = Diet.Piscivore;

            if (Random.Range(0f, 1f) > 0.5f)
            {
                int hmm = Random.Range(0, 3);

                if (hmm == 1)
                {
                    GameObject hairAni = GameObject.Instantiate(animator.gameObject);
                    hairAni.GetComponent<SpriteRenderer>().sortingOrder = animator.GetComponent<SpriteRenderer>().sortingOrder + 1;
                    hairAni.transform.SetParent(transform);
                    hairAni.transform.localPosition = animator.transform.localPosition;
                    hairAni.transform.localRotation = animator.transform.localRotation;
                    hairAni.transform.localScale = animator.transform.localScale;

                    Animator hair = hairAni.GetComponent<Animator>();
                    hair.runtimeAnimatorController = pisciHair;
                    accPath = "Profiles/PisciHair";

                    details.Add(hair);
                } else if (hmm == 2)
                {
                    GameObject shirtAni = GameObject.Instantiate(animator.gameObject);
                    shirtAni.GetComponent<SpriteRenderer>().sortingOrder = animator.GetComponent<SpriteRenderer>().sortingOrder + 1;
                    shirtAni.transform.SetParent(transform);
                    shirtAni.transform.localPosition = animator.transform.localPosition;
                    shirtAni.transform.localRotation = animator.transform.localRotation;
                    shirtAni.transform.localScale = animator.transform.localScale;

                    Animator shirt = shirtAni.GetComponent<Animator>();
                    shirt.runtimeAnimatorController = pisciHair;

                    details.Add(shirt);
                } else if (hmm == 3)
                {
                    GameObject hairAni = GameObject.Instantiate(animator.gameObject);
                    hairAni.GetComponent<SpriteRenderer>().sortingOrder = animator.GetComponent<SpriteRenderer>().sortingOrder + 1;
                    hairAni.transform.SetParent(transform);
                    hairAni.transform.localPosition = animator.transform.localPosition;
                    hairAni.transform.localRotation = animator.transform.localRotation;
                    hairAni.transform.localScale = animator.transform.localScale;

                    Animator hair = hairAni.GetComponent<Animator>();
                    hair.runtimeAnimatorController = pisciHair;
                    accPath = "Profiles/PisciHair";

                    details.Add(hair);

                    GameObject shirtAni = GameObject.Instantiate(animator.gameObject);
                    shirtAni.GetComponent<SpriteRenderer>().sortingOrder = animator.GetComponent<SpriteRenderer>().sortingOrder + 1;
                    shirtAni.transform.SetParent(transform);
                    shirtAni.transform.localPosition = animator.transform.localPosition;
                    shirtAni.transform.localRotation = animator.transform.localRotation;
                    shirtAni.transform.localScale = animator.transform.localScale;

                    Animator shirt = shirtAni.GetComponent<Animator>();
                    shirt.runtimeAnimatorController = pisciShirt;

                    details.Add(shirt);
                }
            }
        }
        else if (whatDino == 2)
        {
            dino = 0;
            int carniColor = Random.Range(0, carniColors.Count - 1);
            //Debug.Log("carniColor = " + carniColor);
            animator.runtimeAnimatorController = carniColors[carniColor];
            headPath += "LargeCarnivore" + carniColor;
            diet = Diet.Carnivore;

            if (Random.Range(0f, 1f) > 0.2f)
            {
                GameObject clothesAni = GameObject.Instantiate(animator.gameObject);
                clothesAni.GetComponent<SpriteRenderer>().sortingOrder = animator.GetComponent<SpriteRenderer>().sortingOrder + 1;
                clothesAni.transform.SetParent(transform);
                clothesAni.transform.localPosition = animator.transform.localPosition;
                clothesAni.transform.localRotation = animator.transform.localRotation;
                clothesAni.transform.localScale = animator.transform.localScale;

                Animator clothes = clothesAni.GetComponent<Animator>();
                int clothesNum = Random.Range(0, carniClothes.Count - 1);
                clothes.runtimeAnimatorController = carniClothes[clothesNum];
                accPath = "Profiles/LargeCarnivoreClothes" + clothesNum;

                details.Add(clothes);
            }
        }
        else if (whatDino == 3)
        {
            dino = 1;
            animator.runtimeAnimatorController = spinoColor;
            headPath += "Spino";
            diet = Diet.Piscivore;
        }
        else if (whatDino == 4)
        {
            dino = 0;
            int raptorColor = Random.Range(0, raptorColors.Count - 1);
            //Debug.Log("raptorColor = " + raptorColor);
            animator.runtimeAnimatorController = raptorColors[raptorColor];
            headPath += "Raptor" + raptorColor;
            diet = Diet.Piscivore;
        }
        else if (whatDino == 5)
        {
            dino = 0;
            animator.runtimeAnimatorController = karen;
            headPath += "Karen";
            diet = Diet.Carnivore;
        }
        else
        {
            dino = 0;
            int killerColor = Random.Range(0, killerColors.Count - 1);
            animator.runtimeAnimatorController = killerColors[killerColor];
            headPath += "Killer" + killerColor;
            diet = Diet.Carnivore;
        }

        order = Recipes.GenerateDish(diet);

        orderTalk = (CharacterTalk)ScriptableObject.CreateInstance(typeof(CharacterTalk));
        orderTalk.charSound = talkingClip;
        //Debug.Log(headPath);
        head = Resources.Load<Texture>(headPath);
        orderTalk.characterHead = head;
        orderTalk.acc = null;
        if (accPath != null)
        {
            acc = Resources.Load<Texture>(accPath);
            //Debug.Log(accPath);
        }
        orderTalk.acc = acc;
        orderTalk.speed = 0.01f;
        orderTalk.color = Color.white;
        orderTalk.text = "Can I have " + order + " please?";
    }

    private void Update()
    {
        switch (state)
        {
            case State.Waiting:
                Waiting();
                break;

            case State.GoingToTable:
                GoToTable();
                break;

            case State.SittingAtTable:
                SitAtTable();
                break;

            case State.Leaving:
                Leave();
                break;
        }
    }

    public void UpdateSprite(Vector2 velocity, bool ignore = false)
    {
        debugCircle.transform.localPosition = velocity / 5f;

        if (velocity.magnitude > 0.01)
        {
            float angle = -Vector2.SignedAngle(Vector2.up, velocity.normalized);
            lastMoveSprite = (MoveSprite)(int)moveSprite;

            if (logVel)
            {
                //Debug.Log(velocity);
                //Debug.Log(angle);
            }

            if (angle > 135 || angle < -135)
            {
                moveSprite = MoveSprite.down;
            }
            else if (angle > 45)
            {
                moveSprite = MoveSprite.right;
            }
            else if (angle < -45)
            {
                moveSprite = MoveSprite.left;
            }
            else
            {
                moveSprite = MoveSprite.up;
            }

            if (lastMoveSprite != moveSprite || ignore)
            {
                animator.Play(moveSprite.ToString(), -1, 0);

                if (moveSprite == MoveSprite.left && dino == 0 || moveSprite == MoveSprite.right && dino == 1)
                {
                    animator.GetComponent<SpriteRenderer>().flipX = true;
                } else
                {
                    animator.GetComponent<SpriteRenderer>().flipX = false;
                }

                foreach (Animator detail in details)
                {
                    detail.Play(moveSprite.ToString(), -1, 0);

                    if (moveSprite == MoveSprite.left && dino == 0 || moveSprite == MoveSprite.right && dino == 1)
                    {
                        detail.GetComponent<SpriteRenderer>().flipX = true;
                    }
                    else
                    {
                        detail.GetComponent<SpriteRenderer>().flipX = false;
                    }
                }
            }
        }
        else
        {
            animator.Play(moveSprite.ToString(), -1, 0);

            if (moveSprite == MoveSprite.left && dino == 0 || moveSprite == MoveSprite.right && dino == 1)
            {
                animator.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                animator.GetComponent<SpriteRenderer>().flipX = false;
            }

            foreach (Animator detail in details)
            {
                detail.Play(moveSprite.ToString(), -1, 0);

                if (moveSprite == MoveSprite.left && dino == 0 || moveSprite == MoveSprite.right && dino == 1)
                {
                    detail.GetComponent<SpriteRenderer>().flipX = true;
                }
                else
                {
                    detail.GetComponent<SpriteRenderer>().flipX = false;
                }
            }
        }

        animator.speed = velocity.magnitude / movementSpeed;

        foreach (Animator detail in details)
        {
            detail.speed = velocity.magnitude / movementSpeed;
        }
    }

    void Waiting()
    {
        state = State.Waiting;
        patience -= Time.deltaTime;
        patienceBar.SetBar(patience / maxPatience);

        UpdateSprite((table.transform.position - target.transform.position).normalized, true);
        animator.Play(moveSprite.ToString() + " idle", -1, 0);

        if (moveSprite == MoveSprite.left && dino == 0 || moveSprite == MoveSprite.right && dino == 1)
        {
            animator.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            animator.GetComponent<SpriteRenderer>().flipX = false;
        }

        foreach (Animator detail in details)
        {
            detail.Play(moveSprite.ToString() + " idle", -1, 0);

            if (moveSprite == MoveSprite.left && dino == 0 || moveSprite == MoveSprite.right && dino == 1)
            {
                detail.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                detail.GetComponent<SpriteRenderer>().flipX = false;
            }
        }

        if (patience <= 0)
        {
            patience = 0;
            Leave();
        }
    }

    void GoToTable()
    {
        state = State.GoingToTable;

        if (target == null || (target.table.busy && target.table.patronGroup != group) && leader && targetTransform != spawner.transform)
        {
            PatronTable newTable = spawner.GetOpenTableFromSize(group.Count);

            if (newTable == null)
            {
                for (int i = 0; i < group.Count; i++)
                {
                    group[i].targetTransform = spawner.transform;
                    group[i].state = State.Leaving;
                }

                //Debug.Log("No Open Tables");
                return;
            }

            List<int> openChairs = new List<int>();

            for (int i = 0; i < newTable.seats.Count; i++)
            {
                openChairs.Add(i);
            }

            for (int i = 0; i < group.Count; i++)
            {
                int chairToGoTo = openChairs[Random.Range(0, openChairs.Count)];
                group[i].target = newTable.seats[chairToGoTo];
                group[i].targetTransform = newTable.seats[chairToGoTo].transform;
                openChairs.Remove(chairToGoTo);
            }
        }

        ai.destination = target.gameObject.GetComponent<Transform>().position;

        UpdateSprite(ai.velocity);

        if (ai.reachedDestination)
        {
            
            state = State.SittingAtTable;
        }
    }

    void SitAtTable()
    {
        state = State.Waiting;

        table = target.table;

        target.occupied = true;
        table.busy = true;

        table.patrons.Add(this);
        table.patronGroup = group;
        table.CheckReady();

        UpdateSprite((table.transform.position - target.transform.position).normalized, true);
        animator.Play(moveSprite.ToString() + " idle", -1, 0);

        if (moveSprite == MoveSprite.left && dino == 0 || moveSprite == MoveSprite.right && dino == 1)
        {
            animator.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            animator.GetComponent<SpriteRenderer>().flipX = false;
        }

        foreach (Animator detail in details)
        {
            detail.Play(moveSprite.ToString() + " idle", -1, 0);

            if (moveSprite == MoveSprite.left && dino == 0 || moveSprite == MoveSprite.right && dino == 1)
            {
                detail.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                detail.GetComponent<SpriteRenderer>().flipX = false;
            }
        }

        patienceBar.gameObject.SetActive(true);
    }

    public void GiveTip()
    {
        double tipMultiplier = 7.0f;
        int totalTip = (int)Math.Round(tipMultiplier * (patience / maxPatience));
        PlayerUpgradeManager.Instance.playerWallet += totalTip;
        patronAudio.PlayOneShot(tipAudio);

        /*if (!hasTipped)
        {
            PlayerUpgradeManager.Instance.playerWallet += totalTip;
            dinoDollar.SetActive(true);
            tipText.GetComponent<TextMeshProUGUI>().SetText(totalTip.ToString());

            patronAudio.PlayOneShot(tipAudio);

            hasTipped = true;
        }
        tipText.transform.position = Camera.main.WorldToScreenPoint(dinoDollar.transform.position);*/
    }

    public void Leave()
    {
        state = State.Leaving;
        targetTransform = spawner.transform;
        ai.destination = targetTransform.position;

        if (patienceBar.gameObject.activeSelf)
        {
            table.patrons.Remove(this);
            target.occupied = false;
            patienceBar.gameObject.SetActive(false);
        }

        UpdateSprite(ai.velocity);

        if (Vector3.Distance(transform.position, targetTransform.position) < 1f)
        {
            Die();
        }
    }

    void Die()
    {
        GameManager.Instance.player.totalServed++;
        spawner.spawned.Remove(this);
        Destroy(gameObject);
    }

    public enum State
    {
        GoingToTable,
        SittingAtTable,
        Waiting,
        Talking,
        Leaving
    }

    public enum Diet
    {
        Vegetarian,
        Carnivore,
        Piscivore
    }
}
