using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpdateWalletNumber : MonoBehaviour
{
    GameObject conversation;
    GameObject uiDinoDollar;

    // Start is called before the first frame update
    void Start()
    {
        uiDinoDollar = GameObject.Find("UI Dino Dollar");
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<TextMeshProUGUI>().SetText("" + PlayerUpgradeManager.Instance.playerWallet);
        if (GameManager.Instance.conversationPlayer.gameObject.activeSelf)
        {
            gameObject.GetComponent<TextMeshProUGUI>().color = Color.clear;
            uiDinoDollar.GetComponent<Image>().color = Color.clear;
        } 
        else
        {
            gameObject.GetComponent<TextMeshProUGUI>().color = Color.white;
            uiDinoDollar.GetComponent<Image>().color = Color.white;
        }
    }
}
