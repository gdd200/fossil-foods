using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatronSeat : MonoBehaviour
{
    public bool occupied = false;
    public PatronTable table;

    void Start()
    {
        table = transform.parent.GetComponent<PatronTable>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "patron")
        {
            occupied = true;
            table.busy = true;

            Patron patron = other.GetComponent<Patron>();
            table.patrons.Add(patron);
            table.patronGroup = patron.group;
            table.CheckReady();

            patron.UpdateSprite((table.transform.position - transform.position).normalized * 0.001f);
        }
    }
}
