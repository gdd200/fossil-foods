using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleMenu : MonoBehaviour
{
    public Button start;
    public Button options;
    public Button back;
    public Button quit;

    public GameObject foot;

    public int overButton = -1;

    public void StartButtonPressed()
    {
        SceneManager.LoadScene("Intro Scene");
    }

    public void OptionsButtonPressed()
    {
        //Debug.Log("no options yet");
    }

    public void QuitButtonPressed()
    {
        Application.Quit(); 
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerUpgradeManager.Instance != null)
        {
            GameObject.Destroy(PlayerUpgradeManager.Instance.gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        switch (overButton)
        {
            case -1:
                foot.gameObject.SetActive(false);
                break;

            case 0:
                foot.gameObject.SetActive(true);
                foot.transform.SetParent(start.transform.parent);
                foot.GetComponent<RectTransform>().anchoredPosition = start.GetComponent<RectTransform>().anchoredPosition + new Vector2(100, 0);
                break;

            case 1:
                foot.gameObject.SetActive(true);
                foot.transform.SetParent(options.transform.parent);
                foot.GetComponent<RectTransform>().anchoredPosition = options.GetComponent<RectTransform>().anchoredPosition + new Vector2(100, 0);
                break;

            case 2:
                foot.gameObject.SetActive(true);
                foot.transform.SetParent(back.transform.parent);
                foot.GetComponent<RectTransform>().anchoredPosition = back.GetComponent<RectTransform>().anchoredPosition + new Vector2(169, 0);
                break;

            case 3:
                foot.gameObject.SetActive(true);
                foot.transform.SetParent(quit.transform.parent);
                foot.GetComponent<RectTransform>().anchoredPosition = quit.GetComponent<RectTransform>().anchoredPosition + new Vector2(100, 0);
                break;
        }
    }

    public void StartOver()
    {
        overButton = 0;
    }

    public void StartOff()
    {
        if (overButton == 0)
        {
            overButton = -1;
        }
    }

    public void OptionsOver()
    {
        overButton = 1;
    }

    public void OptionsOff()
    {
        if (overButton == 1)
        {
            overButton = -1;
        }
    }

    public void BackOver()
    {
        overButton = 2;
    }

    public void BackOff()
    {
        if (overButton == 2)
        {
            overButton = -1;
        }
    }

    public void QuitOver()
    {
        overButton = 3;
    }

    public void QuitOff()
    {
        if (overButton == 3)
        {
            overButton = -1;
        }
    }
}
