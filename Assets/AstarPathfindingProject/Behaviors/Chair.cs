using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Pathfinding
{
 /// <summary>
 /// Sets the destination of an AI to the position of a specified object.
 /// This component should be attached to a GameObject together with a movement script such as AIPath, RichAI or AILerp.
 /// This component will then make the AI move towards the <see cref="target"/> set on this component.
 ///
 /// See: <see cref="Pathfinding.IAstarAI.destination"/>
 ///
 /// [Open online documentation to see images]
 /// </summary>
    public class Chair : MonoBehaviour
    {
        public bool imSat = false;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        void OnTriggerEnter2D(Collider2D other)
        {
            Debug.Log("hello");
            if (other.gameObject.tag == "patron")
            {
                imSat = true;

            }
        }
    }
}
    
