using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Pathfinding {
	/// <summary>
	/// Sets the destination of an AI to the position of a specified object.
	/// This component should be attached to a GameObject together with a movement script such as AIPath, RichAI or AILerp.
	/// This component will then make the AI move towards the <see cref="target"/> set on this component.
	///
	/// See: <see cref="Pathfinding.IAstarAI.destination"/>
	///
	/// [Open online documentation to see images]
	/// </summary>
	[UniqueComponent(tag = "ai.destination")]
	[HelpURL("http://arongranberg.com/astar/docs/class_pathfinding_1_1_a_i_destination_setter.php")]
	public class AIDestinationSetter : VersionedMonoBehaviour {
        /// <summary>The object that the AI should move to</summary>
        public GameObject[] targets;
        public GameObject home;
        public Transform target;
        public bool done = false;
        

        int chairnum;
        IAstarAI ai;

		void OnEnable () {

			ai = GetComponent<IAstarAI>();
            targets = GameObject.FindGameObjectsWithTag("seats");
            home = GameObject.FindGameObjectWithTag("spawner");

            target = Randomchair();

            // Update the destination right before searching for a path as well.
            // This is enough in theory, but this script will also update the destination every
            // frame as the destination is used for debugging and may be used for other things by other
            // scripts as well. So it makes sense that it is up to date every frame.
            if (ai != null) ai.onSearchPath += Update;
           
        }

		void OnDisable () {
			if (ai != null) ai.onSearchPath -= Update;
		}

		/// <summary>Updates the AI's destination every frame</summary>
		void Update () {
			

			if (target != null && ai != null) ai.destination = target.position;
            if (done == true)
            {
                for (int i = 0; i < targets.Length; i++)
                {
                    if(target == targets[i].GetComponent<Transform>())
                    {
                        Chair ch = target.GetComponentInParent<Chair>();
                        ch.imSat = false;
                    }
                }
                
                target = home.GetComponent<Transform>();
            }
		}
		Transform Randomchair()
		{
            List<GameObject> validTargets = new List<GameObject>();
          
            //creates list of seats that arent taken
            for (int i = 0; i < targets.Length; i++)
            {
                Chair ch = targets[i].GetComponent<Chair>();

                if (ch.imSat == false)
                {
                    validTargets.Add(targets[i]);
                }
            }

            int chairnum = Random.Range(0, validTargets.Count - 1);
            Chair thisUn = validTargets[chairnum].GetComponent<Chair>();
            thisUn.imSat = true;
            return validTargets[chairnum].GetComponent<Transform>();

        }
    }
}
