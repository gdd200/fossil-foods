using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class DayManager : MonoBehaviour
{
    public static DayManager Instance;
    public int DayCount = 0;
    public int[] numOfPatron = new int[5] { 10, 15, 20, 25, 40 };
    public List<GameObject> layoutList = new List<GameObject>();
    public PlayerUpgradeManager playerUpgradeManager;
    public GameObject spawner;
    public GameObject EOD;
    public GameObject dayTextBox;
    public PatronSpawner ps;
    public GameObject gm;
    public GameObject diloHead;
    public float headIncrement;
    public float numOfMoves = 0f;
    public float leftPos = -100f;
    public float rightPos = 100f;

    public bool dayover = false;
    public string[] DayText = new string[5] { "1", "2", "3", "4", "5", };

    void Start()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        // Keep loaded to be able to reference
        //DontDestroyOnLoad(gameObject);

        DayCount = PlayerUpgradeManager.Instance.dayCount;

        headIncrement = 200 / numOfPatron[DayCount];

        ChangeDay(DayCount);
        ps.minSpawnTime = 8f;
        ps.maxSpawnTime = 12f;

        ps.DoitNow();
    }
    public void ChangeDay(int d)
    {
        GameObject temp = GameObject.Find("Day " + (d + 1) + " Layout");
        Destroy(temp);
        GameObject newDay = Instantiate(layoutList[d]);
        newDay.transform.SetParent(transform);
        newDay.GetComponentInChildren<Grid>().enabled = true;

        spawner = GameObject.Find("Patron Spawner");
        if (spawner != null)
        {
            ps = spawner.GetComponent<PatronSpawner>();
            gm.GetComponent<GameManager>().patronSpawner = ps;
            ps.numOfPatron = numOfPatron[d];
        }

        GameManager.Instance.player.health = PlayerUpgradeManager.Instance.health;
        GameManager.Instance.player.ticketCount = PlayerUpgradeManager.Instance.ticketCount;
        GameManager.Instance.player.movementSpeed = PlayerUpgradeManager.Instance.speed;
        GameManager.Instance.player.traySize = PlayerUpgradeManager.Instance.traySize;
    }
    public void EndOfDay()
    {
        PlayerUpgradeManager.Instance.dayCount++;
        if (PlayerUpgradeManager.Instance.dayCount < layoutList.Count)
        {
            SceneManager.LoadScene("Liam_Upgrades_Scene", LoadSceneMode.Single);
        }
        else
        {
            SceneManager.LoadScene("Comet Scene");
        }
    }

    public void TurnOn()
    {
        EOD.SetActive(true);
        dayTextBox.GetComponent<TextMeshProUGUI>().text = DayText[DayCount];
    }

    // Update is called once per frame
    void Update()
    {
        if (ps.spawned.Count == 0 && ps.now == true)
        {
            ps.now = false;
            TurnOn();
        }


        diloHead.GetComponent<RectTransform>().anchoredPosition = new Vector2(Mathf.Lerp(leftPos, rightPos, (float)GameManager.Instance.player.totalServed / (float)numOfPatron[DayCount]), diloHead.GetComponent<RectTransform>().anchoredPosition.y);

    }


}
