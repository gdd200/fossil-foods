using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using Unity.VisualScripting;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using UnityEditor;
using TMPro;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{
    public Canvas canvas;
    public Vector2 startPos;
    public Vector2 currentPos;
    public Recipes thing;
    public GameObject dm;
    public GameObject tray;
    public GameManager gm;
    public Texture2D opent;
    public Texture2D closedt;
    public RectTransform tempRectT;
    public bool spawned = false;
    private TextMeshProUGUI textBox;
    private RectTransform rectT;
    private CanvasGroup canvasG;
    private bool onTray = false;


    // Start is called before the first frame update
    void Start()
    {
        textBox = GetComponentInChildren<TextMeshProUGUI>();
        dm = GameObject.Find("Display");
        gm = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }
    void Update()
    {
        if (startPos == null)
        {
            foreach (GameObject slot in dm.GetComponent<DisplayManager>().traySlots)
            {
                if (!slot.GetComponent<Dishslot>().isFull)
                {
                    startPos = slot.GetComponent<RectTransform>().anchoredPosition;
                }
            }
        }

        if (gm.lastUsedInput != PlayerInputDevice.PC)
        {
            if (Input.GetButtonDown("Fire1") && gameObject == EventSystem.current.currentSelectedGameObject)
            {
                if (!onTray)
                {
                    Debug.Log("on tray");
                    foreach (GameObject slot in dm.GetComponent<DisplayManager>().traySlots)
                    {
                        if (slot.GetComponent<Dishslot>().isFull == false)
                        {
                            gameObject.GetComponent<RectTransform>().anchoredPosition = slot.GetComponent<RectTransform>().anchoredPosition;
                            Dishslot ds = slot.GetComponent<Dishslot>();
                            ds.isFull = true;
                            ds.r = thing;
                            ds.food = gameObject;
                            onTray = true;
                            tray = slot;
                        }
                    }
                }
                else
                {
                    Dishslot ds = tray.GetComponent<Dishslot>();
                    ds.isFull = false;
                    ds.r = null;
                    ds.food = null;
                    tray = null;
                    gameObject.GetComponent<RectTransform>().anchoredPosition = startPos;
                    onTray = false;
                }


            }
        }

    }

    // Update is called once per frame
    private void Awake()
    {
        rectT = GetComponent<RectTransform>();
        canvasG = GetComponent<CanvasGroup>();
        startPos = rectT.anchoredPosition;
        currentPos = rectT.anchoredPosition;
        textBox = GetComponentInChildren<TextMeshProUGUI>();

    }
    public void OnEndDrag(PointerEventData eventData)
    {
        Cursor.SetCursor(opent, new Vector2(36, 33), CursorMode.Auto);
        canvasG.alpha = 1.0f;
        canvasG.blocksRaycasts = true;

    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (rectT.parent == dm.GetComponent<DisplayManager>().trayDaddy.GetComponent<RectTransform>())
        {
            tempRectT = dm.GetComponent<DisplayManager>().trayDaddy.GetComponent<RectTransform>();
        }
        rectT.SetParent(GameObject.Find("Kitchen Canvas").GetComponent<RectTransform>());
        Cursor.SetCursor(closedt, new Vector2(36, 33), CursorMode.Auto);
        canvasG.alpha = .6f;
        canvasG.blocksRaycasts = false;
    }
    public void OnDrag(PointerEventData eventData)
    {

        rectT.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }
    public void OnPointerDown(PointerEventData eventData)
    {


    }

    public void OnDrop(PointerEventData eventData)
    {

        gameObject.GetComponentInParent<tray>().OnDrop(eventData);

    }
    public void ShowText()
    {

        textBox.text = thing.ToString();


    }
    public void HideText()
    {

        textBox.text = "";

    }
    public void DisplayDish()
    {
        //Debug.Log(thing.GetDetailedSpritePath()); good for testing
        //Debug.Log(thing.GetSpriteName());
        GetComponent<Image>().sprite = Resources.Load<Sprite>(thing.GetDetailedSpritePath());

        //if (GetComponent<Image>().sprite == null) shouldn't need fallback anymore :)
        //{
        //    GetComponent<Image>().sprite = Resources.Load<Sprite>(thing.GetSpritePath());
        //}

        //Debug.Log(thing.GetDetailedSpritePath());
    }

}
