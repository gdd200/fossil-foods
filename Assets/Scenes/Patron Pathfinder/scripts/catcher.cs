using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class catcher : MonoBehaviour, IDropHandler
{

    public DisplayManager dm;

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            if (eventData.pointerDrag.GetComponent<DragDrop>().spawned == true)
            {
                eventData.pointerDrag.GetComponent<RectTransform>().SetParent(dm.trayDaddy.GetComponent<RectTransform>());
                eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = eventData.pointerDrag.GetComponent<DragDrop>().startPos;
                Debug.Log("Yay");
            }
            else
            {
                Debug.Log("nah");
                eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = eventData.pointerDrag.GetComponent<DragDrop>().startPos;
                dm.trayDaddy.GetComponent<tray>().count--;
            }
            
            
            
        }
    }
}
