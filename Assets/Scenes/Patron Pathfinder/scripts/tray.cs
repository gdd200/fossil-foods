using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class tray : MonoBehaviour, IDropHandler
{
    public GameObject[] slots = new GameObject[0];
    public int count = 0;
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null && count < slots.Length)
        {
            
            if (count == 0)
            {
                eventData.pointerDrag.GetComponent<RectTransform>().SetParent(gameObject.GetComponent<RectTransform>());
                eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = slots[count].GetComponent<RectTransform>().anchoredPosition;
                eventData.pointerDrag.GetComponent<DragDrop>().currentPos = slots[count].GetComponent<RectTransform>().anchoredPosition;
                Debug.Log(eventData.pointerDrag.GetComponent<DragDrop>().currentPos);
                Debug.Log(slots[count].GetComponent<RectTransform>().anchoredPosition);
                slots[count].GetComponent<Dishslot>().food = eventData.pointerDrag;
                slots[count].GetComponent<Dishslot>().r = eventData.pointerDrag.GetComponent<DragDrop>().thing;
                slots[count].GetComponent<Dishslot>().isFull = true;
                count++;
            }
            else if (slots[count].GetComponent<Dishslot>().isFull == false && slots[count-1].GetComponent<Dishslot>().isFull == true)
            {
                eventData.pointerDrag.GetComponent<RectTransform>().SetParent(gameObject.GetComponent<RectTransform>());
                eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = slots[count].GetComponent<RectTransform>().anchoredPosition;
                eventData.pointerDrag.GetComponent<DragDrop>().currentPos = slots[count].GetComponent<RectTransform>().anchoredPosition;
                slots[count].GetComponent<Dishslot>().food = eventData.pointerDrag;
                slots[count].GetComponent<Dishslot>().r = eventData.pointerDrag.GetComponent<DragDrop>().thing;
                slots[count].GetComponent<Dishslot>().isFull = true;
                count++;
            }
            else
            {
                eventData.pointerDrag.GetComponent<RectTransform>().SetParent(gameObject.GetComponent<RectTransform>());
                eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = eventData.pointerDrag.GetComponent<DragDrop>().startPos;
            }

        }


    }
    void Update()
    {
        if(count < 0)
        {
            count = 0;  
        }  
    }
}
