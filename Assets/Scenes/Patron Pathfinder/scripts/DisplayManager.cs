using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayManager : MonoBehaviour
{
    // Start is called before the first frame update
    public List<Recipes> cooked = new List<Recipes>();
    public GameObject[] foodSlots = new GameObject[8];
    public GameObject[] tList = new GameObject[4];
    public List<GameObject> traySlots = new List<GameObject>();
    public GameObject itemPrefab;
    public GameObject canvas;
    public GameObject trayDaddy;
    public Texture2D openCursor;
    public Texture2D closedCursor;
    bool intothevoid = false;

    void Start()
    {
        canvas = GameObject.Find("Kitchen Canvas");
        if (PlayerUpgradeManager.Instance.traySize == 3)
        {
            trayDaddy = tList[0];
        }
        else if (PlayerUpgradeManager.Instance.traySize == 4)
        {
            trayDaddy = tList[1];
        }
        else if (PlayerUpgradeManager.Instance.traySize == 5)
        {
            trayDaddy = tList[2];
        }
        else if (PlayerUpgradeManager.Instance.traySize == 6)
        {
            trayDaddy = tList[3];
        }

        trayDaddy.SetActive(true);
        traySlots.Clear();  
        foreach(GameObject slot in trayDaddy.GetComponent<tray>().slots)
        {
            traySlots.Add(slot);
        }
        for (int i = 0; i < foodSlots.Length; i++)
        {
            foodSlots[i].SetActive(false);
        }
        DisplayNow();
        if (GameManager.Instance.player.deliveringFood.Count > 0)
        {
            int count = 0;
            foreach (Recipes r in GameManager.Instance.player.deliveringFood)
            {
                Vector2 killMe = traySlots[count].GetComponent<RectTransform>().anchoredPosition;
                GameObject hi = Instantiate(itemPrefab, trayDaddy.GetComponent<RectTransform>());
                hi.GetComponent<DragDrop>().canvas = canvas.GetComponent<Canvas>();
                hi.GetComponent<DragDrop>().thing = GameManager.Instance.player.deliveringFood[count];
                hi.transform.SetParent(canvas.transform);
                hi.GetComponent<RectTransform>().SetParent(trayDaddy.GetComponent<RectTransform>());
                hi.GetComponent<RectTransform>().anchoredPosition = killMe;
                hi.GetComponent<RectTransform>().localScale = new Vector2 (1,1);    
                hi.GetComponent<DragDrop>().startPos = killMe;
                hi.GetComponent<DragDrop>().tempRectT = trayDaddy.GetComponent<RectTransform>();
                hi.GetComponent<DragDrop>().spawned = true;
                traySlots[count].GetComponent<Dishslot>().isFull = true;
                traySlots[count].GetComponent<Dishslot>().r = hi.GetComponent<DragDrop>().thing;
                traySlots[count].GetComponent<Dishslot>().food = hi;
                hi.GetComponent<DragDrop>().DisplayDish();
                trayDaddy.GetComponent<tray>().count++;
                count++;
            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckChef();

        if (intothevoid) return;

        if (Input.GetButton("Fire1"))
        {
            Cursor.SetCursor(closedCursor, new Vector2(closedCursor.width / 2f, closedCursor.height / 2f), CursorMode.ForceSoftware);
        } else
        {
            Cursor.SetCursor(openCursor, new Vector2(openCursor.width / 2f, openCursor.height / 2f), CursorMode.ForceSoftware);
        }
    }

    public void CheckChef()
    {
        if (cooked == GameManager.Instance.chef.cooked)
        {
            DisplayNow();
        }
        else
        {
            //Debug.Log("Here");
        }
    }
    public void DisplayNow()
    {
        cooked = GameManager.Instance.chef.cooked;
        if (cooked.Count >= 1 && cooked.Count <= 8)
        {
            for (int i = 0; i < cooked.Count; i++)
            {
                foodSlots[i].SetActive(true);
                foodSlots[i].GetComponent<DragDrop>().thing = cooked[i];
                foodSlots[i].GetComponent<DragDrop>().DisplayDish();
                
            }
        }
       
    }

    //just and edgey name for a function to kill this scene
    public void IntoTheVoid()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        intothevoid = true;

        GameManager.Instance.player.deliveringFood.Clear();

        int count = 0;  
        foreach (GameObject tray in traySlots)
        {
            if (tray.GetComponent<Dishslot>().isFull == true)
            {
                GameManager.Instance.player.deliveringFood.Add(tray.GetComponent<Dishslot>().r);
                GameManager.Instance.chef.cooked.Remove(tray.GetComponent<Dishslot>().r);
            }

            count++;
        }
        GameManager.Instance.UpdatePlayerState(PlayerState.Walking);

    }
}
