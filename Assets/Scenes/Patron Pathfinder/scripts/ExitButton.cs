using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitButton : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject dm;
    public GameObject kitchen;
    void Start()
    {
        kitchen = GameObject.Find("Kitchen");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            dm.GetComponent<DisplayManager>().IntoTheVoid();
            Resources.FindObjectsOfTypeAll<kitchen>()[0].canInteract = true;
            SceneManager.UnloadSceneAsync("Kitchen");
        }
    }
    public void ExitB()
    {
        dm.GetComponent<DisplayManager>().IntoTheVoid();
        Resources.FindObjectsOfTypeAll<kitchen>()[0].canInteract = true;
        SceneManager.UnloadSceneAsync("Kitchen");
    }
}
