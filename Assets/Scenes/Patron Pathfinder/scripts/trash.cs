using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trash : Interactable
{
    public Player player;
    // Start is called before the first frame update
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Interact()
    {
        if (player.takingOrders.Count > 0)
        {
            player.takingOrders[0].origin.needsFood = false;
            player.takingOrders[0].origin.needsToOrder = true;
            player.takingOrders.Remove(player.takingOrders[0]);
        }

        base.Interact();
    }
}
