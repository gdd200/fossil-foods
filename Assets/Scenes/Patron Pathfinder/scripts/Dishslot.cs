using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Dishslot : MonoBehaviour, IDropHandler
{
    public GameObject gm;
    public GameObject food;
    public Recipes r;
    public bool isFull;
    void Start()
    {
        gm = GameObject.Find("Game Manager");
    }
    void Update()
    {
        if (food != null)
        {
            if (food.GetComponent<RectTransform>().anchoredPosition != GetComponent<RectTransform>().anchoredPosition)
            {
                isFull = false;
                r = null;
            }
        }

    }
    public void OnDrop(PointerEventData eventData)
    {

        gameObject.GetComponentInParent<tray>().OnDrop(eventData);


    }
}
