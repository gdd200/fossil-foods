using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class kitchen : Interactable
{
    //for test
    public GameObject Cheif;
    public static GameObject kitchenExclamationPoint;

    void Start()
    {
        
        base.Start();
        kitchenExclamationPoint = new GameObject();
        kitchenExclamationPoint.name = "Chef Exclamation Point";
        kitchenExclamationPoint.transform.parent = transform;
        kitchenExclamationPoint.AddComponent<SpriteRenderer>();
        kitchenExclamationPoint.transform.position = transform.position;
        kitchenExclamationPoint.GetComponent<SpriteRenderer>().color = Color.green;
        kitchenExclamationPoint.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("VanHornMatthias_ExclimationPoint_01");
        kitchenExclamationPoint.GetComponent<SpriteRenderer>().sortingLayerName = "Patience Meter";
        kitchenExclamationPoint.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void Interact()
    {
        if (SceneManager.GetActiveScene().name != "Kitchen")
        {
            popup.SetActive(false);
            SceneManager.LoadScene("Kitchen", LoadSceneMode.Additive);
            GameManager.Instance.UpdatePlayerState(PlayerState.GettingFood);
            canInteract = false;
            base.Interact();

        }
        
    }

}