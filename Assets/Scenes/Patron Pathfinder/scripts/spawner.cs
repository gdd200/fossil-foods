using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    public GameObject[] targets;
    public GameObject patron;
    int count = 0;
    public List<GameObject> children = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        targets = GameObject.FindGameObjectsWithTag("seats");
        InvokeRepeating("SpawnPatron", 0, 5);
        
    }

    // Update is called once per frame
    void Update()
    {
        Transform me = this.GetComponentInParent<Transform>();
        for (int i = 0; i < children.Count; i++) 
        {
            GameObject child = children[i];
            Transform cT = child.GetComponent<Transform>();
            if (cT.position.y <= me.position.y + 0.25f)
            {
                children.Remove(child);
                count--;
                Destroy(child);
            }
        }
    }

    void SpawnPatron()
    {
        if(count < targets.Length)
        {
            children.Add(Instantiate(patron, new Vector3(1.75f, -4.5f, 0), Quaternion.identity));

            count++;
        }
        
    }
}
